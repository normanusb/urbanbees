package com.urbanbees.nextorbis.urbanbeesmobility.challenges.events;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.urbanbees.nextorbis.urbanbeesmobility.R;
import com.urbanbees.nextorbis.urbanbeesmobility.challenges.Info_events_Activity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class EventsFragment extends Fragment {
    String city_name,user_id;
    ListView listView_events;
    FirebaseAuth auth;
    ProgressBar spinner;
    Map<String, String> eventid = new HashMap<String, String>();
    List<String> placesdatesorganized = new ArrayList<>();
    DatabaseReference distanceRef,reference;;
    TextView events_city_name,dialoginformation, dialogtitle;
    ImageButton dialog_button;
    int deviceWidth, deviceHeight;
    LinearLayout textsizecalc,imagelinearlayout1;
    Activity activity1;
    SortedMap<Float,String> placesordered = new TreeMap<Float,String>();
    private FusedLocationProviderClient locationclient;
    Double  place_lat,place_lon;
    Location locationPlace = new Location("Place");
    Location location_person;
    CustomAdapter customAdapter;
    boolean located=false;
    private static final String TAG = "Distances";
    String[] placename, placeInfo,placeImage,placeUrbees,placeIds,placeDate;
    Dialog welcomingDialog1;
    Date currenttime;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_events, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {

        activity1 = getActivity();
        auth = FirebaseAuth.getInstance();
        user_id=auth.getCurrentUser().getUid();
        if(isAdded() && activity1 !=null) {
            //tutorial introduction
            reference = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id);
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                 @Override
                 public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                     String userIsNew = dataSnapshot.child("firsttimechallenges").getValue().toString();
                     if (userIsNew.equals("yes")) {
                         Okdialog(getString(R.string.welcome_title_challenge),getString(R.string.welcome_text_challenge));
                         reference.child("firsttimechallenges").setValue("no");
                     }
                 }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(activity1, databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });


            //Determine width and height of the screen for the images from firebase
            widhandheight thread = new widhandheight();
            thread.start();
            //name of the city
            SharedPreferences sharedPreferences = activity1.getSharedPreferences("city", activity1.MODE_PRIVATE);
            city_name = sharedPreferences.getString("city", "");
            events_city_name = view.findViewById(R.id.City_name_image);
            events_city_name.setVisibility(View.INVISIBLE);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(events_city_name,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
           //TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(events_city_name,25,50,1, TypedValue.COMPLEX_UNIT_PX);

            //events_city_name.setText(getString(R.string.Events_in_city) +" "+ city_name.toString());
            listView_events = view.findViewById(R.id.listview_events);
            //for loading time
            spinner = view.findViewById(R.id.progressBarEvents);
            spinner.setVisibility(View.VISIBLE);

            //calculate distance from the user of each place
            //For the location check
            requestPermission();

            locationclient = LocationServices.getFusedLocationProviderClient(getContext());
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            locationclient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    located = true;
                    location_person = location;
                    distanceRef = FirebaseDatabase.getInstance().getReference();
                    distanceRef.child("Cities").child(city_name).child("Events").orderByChild("expired").equalTo("no").
                            addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    //Algorithm to check wich dates of the events are first to show them by date order
                                    if(placesordered.size()==0) {
                                        Date c = Calendar.getInstance().getTime();
                                        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
                                        String formattedDate = df.format(c);
                                        try{
                                            currenttime=df.parse(formattedDate);
                                        }catch (ParseException e){
                                            e.printStackTrace();
                                        }

                                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                        if(!(snapshot.child("Users").child(user_id).exists()) ||snapshot.child("Users").child(user_id).child("went").getValue().equals("no")) {
                                            String dateofEvent= snapshot.child("date").getValue().toString();
                                           try{
                                               Date dateofeventformatted= df.parse(dateofEvent);
                                               long difference = currenttime.getTime() - dateofeventformatted.getTime();
                                               float daysBetween = -(difference / (1000*60*60*24));
                                               if(daysBetween>=0){
                                                   placesordered.put(daysBetween, snapshot.getKey());
                                               }else{
                                                   snapshot.child("expired").getRef().setValue("yes");
                                               }
                                               Log.d("DAYS FOR EACH EVENT","days in between: " +daysBetween);
                                           }catch (ParseException e){
                                               e.printStackTrace();
                                           }

                                        }
                                    }
                                    }

                                        int amountofplaces = placesordered.size() + 1;
                                        //get the ids in order from the treemap
                                        placename = new String[amountofplaces];
                                        placeInfo = new String[amountofplaces];
                                        placeImage = new String[amountofplaces];
                                        placeUrbees = new String[amountofplaces];
                                        placeIds = new String[amountofplaces];
                                        placeDate = new String[amountofplaces];
                                        int j = 0;
                                        for (Map.Entry<Float, String> entry : placesordered.entrySet()) {
                                            String placeid = entry.getValue();
                                            placeIds[j] = placeid;
                                            placename[j] = dataSnapshot.child(placeid).child("name").getValue().toString();
                                            placeInfo[j] = dataSnapshot.child(placeid).child("description").getValue().toString();
                                            placeImage[j] = dataSnapshot.child(placeid).child("image").getValue().toString();
                                            placeUrbees[j] = dataSnapshot.child(placeid).child("urbees").getValue().toString();
                                            placeDate[j] = dataSnapshot.child(placeid).child("date").getValue().toString();
                                            j++;
                                        }
                                        customAdapter = new CustomAdapter();
                                        listView_events.setAdapter(customAdapter);




                                }


                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Toast.makeText(getContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
                }
            });

     }
    }



    public String getCurrentTimeUsingDate() {
        Date date = new Date();
        String strDateFormat = "dd.MM.yyyy";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
        String formattedDate= dateFormat.format(date);
        return formattedDate;

    }
    //Determine width and height of the screen for the images from firebase
    class widhandheight extends Thread {

        @Override
        public void run() {
            if(isAdded() && activity1 !=null) {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                WindowManager windowmanager = (WindowManager) activity1.getSystemService(Context.WINDOW_SERVICE);
                windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
                deviceWidth = Math.round(displayMetrics.widthPixels );
                deviceHeight = displayMetrics.heightPixels;
            }
        }
    }
    private  void requestPermission(){
        ActivityCompat. requestPermissions(getActivity(),new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},1);
    }

    private class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return placesordered.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1= getLayoutInflater().inflate(R.layout.event_layout3,null);

            final TextView eventname = view1.findViewById(R.id.textView_descript_event);
            final TextView eventinfo = view1.findViewById(R.id.textView_info_event);
            final TextView event_button_text=view1.findViewById(R.id. button_event_check_text);

            TextViewCompat.setAutoSizeTextTypeWithDefaults(eventname,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
           // TextViewCompat.setAutoSizeTextTypeWithDefaults(eventinfo,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);

            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(event_button_text,25,40,1, TypedValue.COMPLEX_UNIT_PX);


            ImageButton eventurbeesButton = view1.findViewById(R.id.button_event_check);
            ImageButton eventurbeesButtonFake= view1.findViewById(R.id.button_event_check_fake);
            TextView productButtonText = view1.findViewById(R.id.button_event_check_text);
            ImageView imageEvent = view1.findViewById(R.id.imageEventPlace);
            imagelinearlayout1 = view1.findViewById(R.id.imagelinearlayout1);
            ViewGroup.LayoutParams params = imagelinearlayout1.getLayoutParams();
            params.width = deviceWidth;
            imagelinearlayout1.setLayoutParams(params);

            eventname.setText(placename[i]);

            String date = placeDate[i];
            if (getCurrentTimeUsingDate().equals(date)) {
                date = "today";
            }
            eventinfo.setText(getString(R.string.date_info)+ " "+date );
            productButtonText.setText(placeUrbees[i]);

            //Picasso.get().load(placeImage[i]).resize(deviceWidth, deviceHeight/6).placeholder(R.drawable.chargepage).into(imageEvent, new com.squareup.picasso.Callback() {Picasso.get().load(placeImage[i]).resize(deviceWidth, deviceHeight/6).placeholder(R.drawable.chargepage).into(imageEvent, new com.squareup.picasso.Callback() {
              Picasso.get().load(placeImage[i]).placeholder(R.drawable.chargepage).into(imageEvent, new com.squareup.picasso.Callback() {
                    @Override
                public void onSuccess() {
                    eventname.setWidth(0);
                    eventinfo.setWidth(0);
                }

                @Override
                public void onError(Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });

            //to get the id
            final String tplacename_value = placename[i];
            eventid.put(tplacename_value, placeIds[i]);

            eventurbeesButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    Intent goeventpage = new Intent(activity1, Info_events_Activity.class);
                    goeventpage.putExtra("eventid", eventid.get(tplacename_value) );
                    goeventpage.putExtra("type", "event");
                    startActivity(goeventpage);

                }
            });
            eventurbeesButtonFake.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    Intent goeventpage = new Intent(activity1, Info_events_Activity.class);
                    goeventpage.putExtra("eventid", eventid.get(tplacename_value) );
                    goeventpage.putExtra("type", "event");
                    startActivity(goeventpage);

                }
            });
            spinner.setVisibility(View.GONE);

            return view1;
        }
    }
    public void Okdialog(String title, String information){
        welcomingDialog1= new Dialog(activity1);
        welcomingDialog1.setContentView(R.layout.popup_notification);

        dialoginformation = welcomingDialog1.findViewById(R.id.text_description);
        dialogtitle = welcomingDialog1.findViewById(R.id.information_text);
        dialog_button= welcomingDialog1.findViewById(R.id.button_ok);

        dialogtitle.setText(title);
        dialoginformation.setText(information);

        dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                welcomingDialog1.dismiss();
            }
        });
        welcomingDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        welcomingDialog1.show();
    }


}


