package com.urbanbees.nextorbis.urbanbeesmobility.profile.viralmarketing;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class sendEmailtoPotencialNewBee extends AsyncTask<Void, Void, Void> {

        private Context context;
        private Session session;

        private String email, subject, messagetosend;

        public sendEmailtoPotencialNewBee(Context context, String email, String subject, String messagetosend) {
            this.context=context;
            this.email= email;
            this.subject=subject;
            this.messagetosend=messagetosend;
        }

    @Override protected void onPreExecute() {
        super.onPreExecute();
        Toast.makeText(context,"Sending invitation..",Toast.LENGTH_LONG).show();
    }


    @Override protected Void doInBackground(Void... voids){
            //properties creation
            Properties props= new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "587");
        props.put("mail.smtp.socketFactory.class", "javax.net.SocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.ssl.enable", "false");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

            //new session
            session= Session.getDefaultInstance(props,
                    new javax.mail.Authenticator(){
                //Authenticating the password
                        protected PasswordAuthentication getPasswordAuthentication(){
                            return new PasswordAuthentication("contacturbanbees@gmail.com", "DoceDeEspadas12.");
                        }
                    });

            try {
              Message message = new MimeMessage(session);
              message.setFrom(new InternetAddress("contacturbanbees@gmail.com"));
              message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(email));
              message.setSubject(subject);
              message.setContent(messagetosend, "text/html; charset=utf-8");
              Transport.send(message);
            }catch (MessagingException e){
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }
           return null;
        }
    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        Toast.makeText(context,"Invitation sent!",Toast.LENGTH_LONG).show();
    }
}

