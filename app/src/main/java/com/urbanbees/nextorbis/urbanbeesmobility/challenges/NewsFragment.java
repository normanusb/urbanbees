package com.urbanbees.nextorbis.urbanbeesmobility.challenges;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.urbanbees.nextorbis.urbanbeesmobility.Articles.News_article_Activity;
import com.urbanbees.nextorbis.urbanbeesmobility.R;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;


public class NewsFragment extends Fragment {
    ListView listView_news;
    FirebaseAuth auth;
    ProgressBar spinner;
    Map<String, String> Newsid = new HashMap<String, String>();
    SortedMap<Float,String> newsordered = new TreeMap<Float,String>();
    DatabaseReference reference,distanceRef;
    TextView News_city_name,dialoginformation, dialogtitle;;;
    String city_name,user_id;
    LinearLayout imagelinearlayout;
    int deviceWidth, deviceHeight,counter;
    Activity activity1;
    private FusedLocationProviderClient locationclient;
    Location location_person;
    NewsFragment.CustomAdapter customAdapter;
    boolean located=false;
    private static final String TAG = "Distances";
    String[] Newsname, NewsInfo,NewsImage,NewsUrbees,NewsIds, Newsclaimed;
    ImageButton dialog_button;
    Dialog welcomingDialog1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news, container, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState) {
        counter=0;
        activity1 = getActivity();
        auth = FirebaseAuth.getInstance();
        user_id=auth.getCurrentUser().getUid();
        if (isAdded() && activity1 != null) {
            //tutorial introduction
            reference = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id);
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String userIsNew = dataSnapshot.child("firsttimenews").exists()? dataSnapshot.child("firsttimenews").getValue().toString():"yes";
                    if ( userIsNew.equals("yes") ) {
                        Okdialog(getString(R.string.welcome_title_places),getString(R.string.appNewsSectionInfo));
                        reference.child("firsttimenews").setValue("no");
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(activity1, databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
            //Determine width and height of the screen for the images from firebase
            NewsFragment.widhandheight thread = new NewsFragment.widhandheight();
            thread.start();
            //name of the city
            SharedPreferences sharedPreferences = activity1.getSharedPreferences("city", activity1.MODE_PRIVATE);
            city_name = sharedPreferences.getString("city", "");
            News_city_name = view.findViewById(R.id.City_name_image3);
            News_city_name.setText(getString(R.string.news_for_city_text) + " " + city_name+" ");
            News_city_name.setVisibility(View.INVISIBLE);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(News_city_name,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            listView_news = view.findViewById(R.id.listview_news);

            //for loading time
            spinner = view.findViewById(R.id.progressBarnews);
            spinner.setVisibility(View.VISIBLE);
            //calculate distance from the user of each place
            //For the location check
            requestPermission();

            locationclient = LocationServices.getFusedLocationProviderClient(getContext());
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }

            locationclient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    located = true;
                    location_person = location;
                    distanceRef = FirebaseDatabase.getInstance().getReference();
                    distanceRef.child("appnews").orderByChild("expired").equalTo("no").
                            addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                        float i=0;
                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                                            newsordered.put(i, snapshot.getKey());
                                            i++;
                                        }

                                    int amountofnews = newsordered.size();

                                    //get the ids in order from the treemap
                                    Newsname = new String[amountofnews];
                                    NewsInfo = new String[amountofnews];
                                    NewsImage = new String[amountofnews];
                                    NewsUrbees = new String[amountofnews];
                                    NewsIds = new String[amountofnews];
                                    Newsclaimed = new String[amountofnews];
                                    int j = 0;
                                    for (Map.Entry<Float, String> entry : newsordered.entrySet()) {
                                        String newsid = entry.getValue();
                                        NewsIds[j] = newsid;
                                        Newsname[j] = dataSnapshot.child(newsid).child("name").getValue().toString();
                                        NewsInfo[j] = dataSnapshot.child(newsid).child("text").getValue().toString();
                                        NewsImage[j] = dataSnapshot.child(newsid).child("image").getValue().toString();
                                        NewsUrbees[j] = dataSnapshot.child(newsid).child("urbees").getValue().toString();

                                        if(dataSnapshot.child(newsid).child("users").child(user_id).child("claimed").exists() && dataSnapshot.child(newsid).child("users").child(user_id).child("claimed").getValue().toString().equals("yes")){
                                            Newsclaimed[j] = "yes";
                                        }else{
                                            Newsclaimed[j] = "no";
                                        }

                                        j++;
                                    }

                                    customAdapter = new CustomAdapter();
                                    listView_news.setAdapter(customAdapter);

                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                }
            });
        }
    }

    //Determine width and height of the screen for the images from firebase
    class widhandheight extends Thread {

        @Override
        public void run() {
            if (isAdded() && activity1 != null) {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                WindowManager windowmanager = (WindowManager) activity1.getSystemService(Context.WINDOW_SERVICE);
                windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
                deviceWidth = Math.round(displayMetrics.widthPixels );
                deviceHeight = displayMetrics.heightPixels;
            }

        }
    }

    private  void requestPermission(){
        ActivityCompat. requestPermissions(getActivity(),new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},1);
    }

    private class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return newsordered.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1= getLayoutInflater().inflate(R.layout.places_layout,null);

            final TextView newsname = view1.findViewById(R.id.textView_descript_event);
            final TextView newsinfo = view1.findViewById(R.id.textView_info_event);
            final TextView news_button_text=view1.findViewById(R.id. button_event_check_text);

            TextViewCompat.setAutoSizeTextTypeWithDefaults(newsname,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(newsinfo,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(news_button_text,25,40,1, TypedValue.COMPLEX_UNIT_PX);

            ImageButton newsurbeesButton = view1.findViewById(R.id.button_event_check);
            ImageButton newsurbeesButtonFake=view1.findViewById(R.id.button_event_check_fake);
            TextView productButtonText = view1.findViewById(R.id.button_event_check_text);
            ImageView imagenews = view1.findViewById(R.id.imageEventPlace);
            imagelinearlayout = view1.findViewById(R.id.imagelinearlayout);
            ViewGroup.LayoutParams params = imagelinearlayout.getLayoutParams();
            params.width = deviceWidth;
            imagelinearlayout.setLayoutParams(params);

            newsname.setText(Newsname[i]);
            newsinfo.setText(getString(R.string.check_out_News_text));
            productButtonText.setText(NewsUrbees[i]);

            if(Newsclaimed[i].equals("yes")){
                newsurbeesButtonFake.setImageResource(R.drawable.claimed);
                news_button_text.setText(getString(R.string.alradyclaimedarticle));
                news_button_text.setTextColor(Color.parseColor("#ffffff"));
            }

            Picasso.get().load(NewsImage[i]).resize(deviceWidth*3, deviceHeight/2).placeholder(R.drawable.chargepage).into(imagenews, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    newsname.setWidth(0);
                    newsinfo.setWidth(0);
                }

                @Override
                public void onError(Exception e) {

                }
            });

            //to get the id
            final String tplacename_value = Newsname[i];
            Newsid.put(tplacename_value, NewsIds[i]);

            newsurbeesButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {


                    Intent gonewspage = new Intent(activity1, News_article_Activity.class);
                    gonewspage.putExtra("newsid", Newsid.get(tplacename_value) );
                    gonewspage.putExtra("type", "News");
                    startActivity(gonewspage);

                }
            });
            newsurbeesButtonFake.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {


                    Intent gonewspage = new Intent(activity1, News_article_Activity.class);
                    gonewspage.putExtra("newsid", Newsid.get(tplacename_value) );
                    gonewspage.putExtra("type", "News");
                    startActivity(gonewspage);

                }
            });
            spinner.setVisibility(View.GONE);

            return view1;
        }
    }
    public void Okdialog(String title, String information){
        welcomingDialog1= new Dialog(activity1);
        welcomingDialog1.setContentView(R.layout.popup_notification);

        dialoginformation = welcomingDialog1.findViewById(R.id.text_description);
        dialogtitle = welcomingDialog1.findViewById(R.id.information_text);
        dialog_button= welcomingDialog1.findViewById(R.id.button_ok);

        dialogtitle.setText(title);
        dialoginformation.setText(information);

        dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                welcomingDialog1.dismiss();
            }
        });
        welcomingDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        welcomingDialog1.show();
    }
}
