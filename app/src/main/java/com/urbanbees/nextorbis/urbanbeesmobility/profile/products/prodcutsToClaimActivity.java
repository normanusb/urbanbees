package com.urbanbees.nextorbis.urbanbeesmobility.profile.products;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.urbanbees.nextorbis.urbanbeesmobility.R;
import com.urbanbees.nextorbis.urbanbeesmobility.store.InfoActivity;

import java.util.ArrayList;

public class prodcutsToClaimActivity extends AppCompatActivity {
    private static final String TAG = "WORKS";
    String products_to_claim_name;
    ListView listView_products;
    FirebaseUser user;
    FirebaseAuth auth;
    ProgressBar spinner;
    ArrayList<String> productid;
    ArrayList<String> productcity;
    ArrayList<String> productname;
    ArrayAdapter<String> adapter;
    DatabaseReference reference;
    TextView products_to_claim_name_text;
    String user_id;
    int deviceWidth, deviceHeight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prodcuts_to_claim);
        //Determine width and height of the screen for the images from firebase
        widhandheight thread = new widhandheight();
        thread.start();
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        user_id=user.getUid();
        listView_products =  findViewById(R.id.listview_products_to_claim);
        products_to_claim_name_text=findViewById(R.id.prodcuts_to_claim_name);
        TextViewCompat.setAutoSizeTextTypeWithDefaults(products_to_claim_name_text,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        //for loading time
        spinner = findViewById(R.id.progressBarProductsToClaim);
        //for loading time
        spinner.setVisibility(View.VISIBLE);
        reference= FirebaseDatabase.getInstance().getReference();
        productname=new ArrayList<String>();
        productcity=new ArrayList<String>();
        productid=new ArrayList<String>();
        reference.child("Users").child(user_id)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        products_to_claim_name_text.setText(R.string.Productstitle);
                        if (dataSnapshot.child("products").exists()) {
                            boolean hasproducts=false;
                        for (DataSnapshot snapshot : dataSnapshot.child("products").getChildren()) {
                            int claimed_value = Integer.parseInt(snapshot.child("claimed").getValue().toString());
                            if (claimed_value > 0) {
                                hasproducts=true;
                                for (int x = 0; x < claimed_value; x++) {
                                    productname.add("Claim: "+snapshot.child("name").getValue().toString());
                                    productcity.add(snapshot.child("city").getValue().toString());
                                    productid.add(snapshot.getKey());
                                }
                            }

                        }
                            if(!hasproducts){
                                Toast.makeText(getApplicationContext(), getString(R.string.no_products_to_claim), Toast.LENGTH_LONG).show();
                                finish();
                            }
                        adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.productstoclaimlayout,  productname);
                        listView_products.setAdapter(adapter);
                        listView_products.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {

                                Intent goPrdocuctPage = new Intent(getApplicationContext(), InfoActivity.class);
                                goPrdocuctPage.putExtra("productID", productid.get(position));
                                goPrdocuctPage.putExtra("type", "productToClaim");
                                goPrdocuctPage.putExtra("cityProduct", productcity.get(position));
                                startActivity(goPrdocuctPage);
                                finish();

                            }
                        });

                        spinner.setVisibility(View.INVISIBLE);
                    }else {
                            Toast.makeText(getApplicationContext(), getString(R.string.no_products_to_claim), Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getApplicationContext(), databaseError.getMessage() + "Enable to retrieve products", Toast.LENGTH_SHORT).show();
                    }
                });


            }

    //Determine width and height of the screen for the images from firebase
    class widhandheight extends Thread {

        @Override
        public void run() {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager windowmanager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
            deviceWidth = Math.round(displayMetrics.widthPixels/2);
            deviceHeight= displayMetrics.heightPixels;

        }
    }

}
