package com.urbanbees.nextorbis.urbanbeesmobility.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.urbanbees.nextorbis.urbanbeesmobility.R;
import com.urbanbees.nextorbis.urbanbeesmobility.main.AppActivity;

import java.util.Arrays;

public class PathActivity extends AppCompatActivity {

    ImageButton signin, signup;
     SignInButton Buttongoogle;
     FirebaseAuth auth;
     FirebaseUser user;
     GoogleSignInClient mGoogleSignInClient;
     LoginButton loginButton;
    DatabaseReference rootReference;
    CallbackManager callbackManager;
    AlertDialog.Builder datenschutz;
    TextView logingbutton_text, registerbutton_text;
    boolean datapolicy= false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        rootReference = FirebaseDatabase.getInstance().getReference().child("Users");
        auth= FirebaseAuth.getInstance();
        user= auth.getCurrentUser();
        if(user == null){
            setContentView(R.layout.activity_path);
            FacebookSdk.sdkInitialize(getApplicationContext());
            callbackManager= CallbackManager.Factory.create();

            //Alert dialog for reservations
            datenschutz = new AlertDialog.Builder(PathActivity.this,R.style.AlertDialogTheme);
            datenschutz.setMessage(getString(R.string.datenschutzenglish)).setCancelable(false)
                    .setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            datapolicy=true;
                            dialogInterface.cancel();
                            FirebaseUser userr = auth.getCurrentUser();

                            User myUserInsertObj = new User(userr.getEmail(), userr.getDisplayName(), "yes", "yes", "yes", "yes","yes", 0, 0, 0, 0);
                            rootReference.setValue(myUserInsertObj)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(getApplicationContext(), "User data saved successfully", Toast.LENGTH_SHORT).show();
                                                finish();
                                                Intent intent = new Intent(getApplicationContext(), AppActivity.class);
                                                startActivity(intent);
                                            } else {
                                                String e = task.getException().toString();
                                                Log.d("Database failed", e + "");

                                                Toast.makeText(getApplicationContext(), "User not saved in database ", Toast.LENGTH_SHORT).show();

                                            }
                                        }
                                    });
                        }
                    });


            loginButton= findViewById(R.id.button_Facebook);
            loginButton.setVisibility(View.GONE);
            loginButton.setEnabled(false);
            signup= findViewById(R.id.button_signup);
            signin= findViewById(R.id.button_signin);
            Buttongoogle = findViewById(R.id.button_google);
            logingbutton_text=findViewById(R.id.pedro_por_su_casa_text);
            registerbutton_text=findViewById(R.id.pedro_por_su_casa_text2);
            loginButton.setReadPermissions(Arrays.asList("email"));

            signin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    signinn();
                }
            });
            signup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    signup();
                }
            });
            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    signfacebook();
                }
            });

            Buttongoogle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    signgoogle();
                }
            });

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();

            mGoogleSignInClient= GoogleSignIn.getClient(this,gso);

            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(logingbutton_text,25,50,1, TypedValue.COMPLEX_UNIT_PX);
            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(registerbutton_text,25,40,1, TypedValue.COMPLEX_UNIT_PX);

        }else{
            finish();
            Intent intent = new Intent(PathActivity.this, AppActivity.class);
            startActivity(intent);

        }



    }



    public void  signinn () {
         Intent intent = new Intent(this, SigninActivity.class);
         startActivity(intent);

    }
    public void  signup () {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);

    }

    public void  signfacebook () {

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                handleFacebookToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "User cancelled login", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    public void signgoogle() {

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 101);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == 101) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Toast.makeText(getApplicationContext(), ""+e, Toast.LENGTH_LONG).show();

            }
        }else{
            callbackManager.onActivityResult(requestCode,resultCode,data);

        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            FirebaseUser user = auth.getCurrentUser();

                            rootReference= FirebaseDatabase.getInstance().getReference().child("Users").child(user.getUid());


                            rootReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    //check if user was already created and if not add  it to database
                                    createuser(dataSnapshot);

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();
                                }
                            });




                        } else {

                            Toast.makeText(getApplicationContext(), "User login failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }
public void createuser(DataSnapshot dataSnapshot){

        boolean exists= dataSnapshot.child("urbees").exists();

        if(!exists){
            AlertDialog alert = datenschutz.create();
            alert.setTitle(getString(R.string.datenschutzenglishTitle));
            alert.show();
            /*if (datapolicy) {
                FirebaseUser userr = auth.getCurrentUser();

                User myUserInsertObj = new User(userr.getEmail(), userr.getDisplayName(), "yes", "yes", "yes", "yes", 0, 0, 0, 0);
                rootReference.setValue(myUserInsertObj)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(getApplicationContext(), "User data saved successfully", Toast.LENGTH_SHORT).show();

                                } else {
                                    String e = task.getException().toString();
                                    Log.d("Database failed", e + "");

                                    Toast.makeText(getApplicationContext(), "User not saved in database ", Toast.LENGTH_SHORT).show();

                                }
                            }
                        });

            }else{

            }*/

        }else{
            finish();
            Intent intent = new Intent(getApplicationContext(), AppActivity.class);
            startActivity(intent);
        }
}


    public void handleFacebookToken(AccessToken accessToken){

        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = auth.getCurrentUser();

                            rootReference= FirebaseDatabase.getInstance().getReference().child("Users").child(user.getUid());
                             // after the user is sign in, add the rest of the user important info
                            rootReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    //check if user was already created and if not add  it to database
                                    createuser(dataSnapshot);

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();
                                }
                            });



                            finish();
                            Intent intent = new Intent(getApplicationContext(), AppActivity.class);
                            startActivity(intent);

                        } else {

                            Toast.makeText(getApplicationContext(), "User login failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }


}
