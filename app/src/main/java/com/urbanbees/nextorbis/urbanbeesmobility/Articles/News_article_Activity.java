package com.urbanbees.nextorbis.urbanbeesmobility.Articles;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.urbanbees.nextorbis.urbanbeesmobility.R;

public class News_article_Activity extends AppCompatActivity {

    //database variables
    FirebaseAuth auth;
    FirebaseUser user;
    DatabaseReference reference, reference2;
    DataSnapshot mainSnapshop;

    ImageButton claim_news_button,link_news_button,dialog_button;
    ImageView news_image_info;
    TextView txtName_news,button_news_claim_text,dialoginformation, dialogtitle ;
    WebView txtInfo_news;
    int deviceWidth, deviceHeight,correctAnswer;

    String type,news_urbees,
            user_read, newsId,
            news_name,
            news_text,news_image,
            user_id,rating_value,answer1,answer2,answer3,question,correctAnswerText;
    Dialog questionDialog,welcomingDialog1;
    //popup question dialog vairables:
    TextView question_text, answer1_text_description, answer2_text_description, answer3_text_description;
    ImageButton answer1_button ,answer2_button ,answer3_button;
    ImageView close_cross;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_news_article_);

        //Determine width and height of the screen for the images from firebase
       widhandheight thread = new widhandheight();
        thread.start();
        //to check if user already claimed points for this news
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        user_id=user.getUid();

        //setting items in the view
        claim_news_button = findViewById(R.id.claim_news_button);
        link_news_button = findViewById(R.id.link_news_button);
        news_image_info = findViewById(R.id.news_image_info);
        txtName_news = findViewById(R.id.txtName_news);
        txtInfo_news = findViewById(R.id.txtInfo_news);
        button_news_claim_text= findViewById(R.id.button_news_claim_text);

        TextViewCompat.setAutoSizeTextTypeWithDefaults(txtName_news,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);

        //getting what type of view it will be
        type = getIntent().getStringExtra("type");

        if (type.equals("News")) {


            reference = FirebaseDatabase.getInstance().getReference().child("appnews");
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //variables from the News article
                        newsId  = getIntent().getStringExtra("newsid");
                        mainSnapshop= dataSnapshot.child(newsId);
                        news_urbees = mainSnapshop.child("urbees").getValue().toString();
                        news_name = mainSnapshop.child("name").getValue().toString();
                        news_text ="<html><body><p align=\"justify\">"+ mainSnapshop.child("text").getValue().toString()+"</p></body><br><br></html>";
                        news_image = mainSnapshop.child("image").getValue().toString();

                        answer1= mainSnapshop.child("answers").child("1").getValue().toString();
                        answer2= mainSnapshop.child("answers").child("2").getValue().toString();
                        answer3= mainSnapshop.child("answers").child("3").getValue().toString();
                        question= mainSnapshop.child("answers").child("question").getValue().toString();
                        correctAnswer= Integer.parseInt(mainSnapshop.child("answers").child("correct").getValue().toString());

                        link_news_button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String link= mainSnapshop.child("link").getValue().toString();
                                Intent browsermaps3= new Intent (Intent.ACTION_VIEW, Uri.parse(link));
                                startActivity(browsermaps3);
                            }
                        });

                        txtName_news.setText(news_name);
                        txtInfo_news.loadData(news_text,"text/html","utf-8");
                        Picasso.get().load(news_image).resize(deviceWidth, deviceWidth*70/100).into(news_image_info);
                        //to check if the user already claimed the urbees or is reserved
                        boolean user_exist= false;
                        if(mainSnapshop.child("users").child(user_id).exists()&& mainSnapshop.child("users").child(user_id).child("claimed").exists()) {
                            user_exist = mainSnapshop.child("users").child(user_id).child("claimed").getValue().toString().equals("yes");
                        }
                        if(user_exist){
                            claim_news_button.setEnabled(false);
                            button_news_claim_text.setText(getString(R.string.appNewsRatedbuttontext));
                        }else {

                               switch (correctAnswer){
                                   case 1:
                                       correctAnswerText= answer1;
                                       break;
                                   case 2:
                                       correctAnswerText= answer2;
                                       break;
                                   case 3:
                                       correctAnswerText= answer3;
                                       break;
                                   default:
                                       correctAnswerText= "";
                               }

                            //question layout setup
                            questionDialog= new Dialog(News_article_Activity.this);

                            questionDialog.setContentView(R.layout.popup_question);

                            question_text= questionDialog.findViewById(R.id.question_text);
                            answer1_text_description= questionDialog.findViewById(R.id.answer1_text_description);
                            answer2_text_description= questionDialog.findViewById(R.id.answer2_text_description);
                            answer3_text_description= questionDialog.findViewById(R.id.answer3_text_description);
                            answer1_button= questionDialog.findViewById(R.id.answer1);
                            answer2_button= questionDialog.findViewById(R.id.answer2);
                            answer3_button= questionDialog.findViewById(R.id.answer3);
                            close_cross= questionDialog.findViewById(R.id.close_cross);

                            question_text.setText(question);
                            answer1_text_description.setText("1- "+answer1);
                            answer2_text_description.setText("2- "+answer2);
                            answer3_text_description.setText("3- "+answer3);
                            close_cross.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    questionDialog.dismiss();
                                }
                            });
                            answer1_button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    questionDialog.dismiss();
                                    givepointsAfterRating(answer1);
                                }
                            });

                            answer2_button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    questionDialog.dismiss();
                                    givepointsAfterRating(answer2);
                                }
                            });

                            answer3_button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    questionDialog.dismiss();
                                    givepointsAfterRating(answer3);
                                }
                            });

                            questionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));




                            //button "the user reads and claims points" functionality
                            claim_news_button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    questionDialog.show();


                                }
                            });

                        }

                    }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });

        }


    }

    //Determine width and height of the screen for the images from firebase
    class widhandheight extends Thread {

        @Override
        public void run() {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager windowmanager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
            deviceWidth = Math.round(displayMetrics.widthPixels*90/100);
            deviceHeight= displayMetrics.heightPixels;

        }
    }

    public void givepointsAfterRating(String answer){
    if(answer.equals(correctAnswerText)) {

            reference2 = FirebaseDatabase.getInstance().getReference();
            reference2.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    int urbees_to_claim = 0;
                    if (dataSnapshot.child("appnews").child(newsId).child("users").child(user_id).exists()) {
                        if (dataSnapshot.child("appnews").child(newsId).child("users").child(user_id).child("try").getValue().toString().equals("0")) {
                            urbees_to_claim = Integer.parseInt(news_urbees);
                            reference2.child("appnews").child(newsId).child("users").child(user_id).child("claimed").setValue("yes");
                        }else{
                            urbees_to_claim = Math.round(Integer.parseInt(news_urbees)/2);
                            reference2.child("appnews").child(newsId).child("users").child(user_id).child("claimed").setValue("yes");
                        }

                    } else {
                        urbees_to_claim = Integer.parseInt(news_urbees);
                        reference2.child("appnews").child(newsId).child("users").child(user_id).child("try").setValue(0);
                        reference2.child("appnews").child(newsId).child("users").child(user_id).child("claimed").setValue("yes");


                    }
                    int OldUrbees = Integer.parseInt(dataSnapshot.child("Users").child(user_id).child("urbees").getValue().toString());
                    int newurbees = urbees_to_claim + OldUrbees;
                    dataSnapshot.child("Users").child(user_id).child("urbees").getRef().setValue(newurbees);

                    if( dataSnapshot.child("Users").child(user_id).child("learningtimes").exists()) {
                        int learningtimes = Integer.parseInt(dataSnapshot.child("Users").child(user_id).child("learningtimes").getValue().toString());
                        String learningtimesnew= Integer.toString(learningtimes+1);
                        dataSnapshot.child("Users").child(user_id).child("learningtimes").getRef().setValue(learningtimesnew);
                    }else{
                        dataSnapshot.child("Users").child(user_id).child("learningtimes").getRef().setValue(1);
                    }

                    Okdialog(getString(R.string.congratulations_urbees_erned_event_title1) + " " + urbees_to_claim + " " + getString(R.string.congratulations_urbees_erned_event_title2),getString(R.string.congratulations_urbees_erned_event_text),true);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });

        }else{
            reference2 = FirebaseDatabase.getInstance().getReference();
            reference2.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (dataSnapshot.child("appnews").child(newsId).child("users").child(user_id).exists()) {
                        int oldtry = Integer.parseInt(dataSnapshot.child("appnews").child(newsId).child("users").child(user_id).child("try").getValue().toString());
                        int newtry= oldtry+1;
                        dataSnapshot.child("appnews").child(newsId).child("users").child(user_id).child("try").getRef().setValue(newtry);
                        }else{
                        reference2.child("appnews").child(newsId).child("users").child(user_id).child("try").setValue(1);
                        reference2.child("appnews").child(newsId).child("users").child(user_id).child("claimed").setValue("no");
                        }
                    Okdialog(getString(R.string.failed_learning_answer_title),getString(R.string.failed_learning_answer),true);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });


        }
    }
    public void Okdialog(String title, String information, final boolean cancel){
        welcomingDialog1= new Dialog(News_article_Activity.this);
        welcomingDialog1.setContentView(R.layout.popup_notification);

        dialoginformation = welcomingDialog1.findViewById(R.id.text_description);
        dialogtitle = welcomingDialog1.findViewById(R.id.information_text);
        dialog_button= welcomingDialog1.findViewById(R.id.button_ok);

        dialogtitle.setText(title);
        dialoginformation.setText(information);

        dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                welcomingDialog1.dismiss();
                if(cancel){
                    finish();
                }
            }
        });
        welcomingDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        welcomingDialog1.show();
    }
}
