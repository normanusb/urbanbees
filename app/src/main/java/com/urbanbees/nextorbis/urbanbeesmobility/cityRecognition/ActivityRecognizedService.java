package com.urbanbees.nextorbis.urbanbeesmobility.cityRecognition;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;

public class ActivityRecognizedService extends IntentService {
    private static final String TAG = "ActivityRecognized";
    long startTime_walk, startTime_run, startTime_byke;
    int transitiontype;
    public ActivityRecognizedService(){
        super(TAG);
    }
    public ActivityRecognizedService(String name){
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(ActivityRecognitionResult.hasResult(intent)){
            ActivityRecognitionResult result= ActivityRecognitionResult.extractResult(intent);
            handleDetectedActivity(result.getProbableActivities());
        }
    }

    public void savepoints(int urbees, String type){
        SharedPreferences sharedPreferences = getSharedPreferences("Pointshandling", MODE_PRIVATE);
        if(type.equals("bike")) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            int urbeestotal= sharedPreferences.getInt("urbees_bike",0)+urbees;
            editor.putInt("urbees_bike",urbeestotal);
            editor.apply();
        }else if(type.equals("foot")){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            int urbeestotal= sharedPreferences.getInt("urbees_foot",0)+urbees;
            editor.putInt("urbees_foot",urbeestotal);
            editor.apply();
        }

    }
    private void handleDetectedActivity (List<DetectedActivity>probableActivities){
        for(DetectedActivity activity: probableActivities) {
            switch (activity.getType()) {
                case DetectedActivity.STILL:{
                    Log.d(TAG,"handleDetectedActvty: STILL"+activity.getConfidence());
                    break;
                }
                case DetectedActivity.WALKING:{

                   if(activity.getConfidence()>=70){
                       savepoints(1,"foot");
                    }


                    Log.d(TAG,"handleDetectedActvty: WALKING"+activity.getConfidence());
                    break;
                }
                case DetectedActivity.ON_FOOT:{
                    if(activity.getConfidence()>=70){
                        savepoints(1,"foot");
                    }
                    Log.d(TAG,"handleDetectedActvty: ON_FOOT"+activity.getConfidence());
                    break;
                }
                case DetectedActivity.RUNNING:{
                    if(activity.getConfidence()>=70){
                        savepoints(1,"foot");
                    }
                    Log.d(TAG,"handleDetectedActvty: RUNNING"+activity.getConfidence());
                    break;
                }
                case DetectedActivity.ON_BICYCLE:{
                    if(activity.getConfidence()>=70){
                        savepoints(1,"bike");
                    }
                    Log.d(TAG,"handleDetectedActvty: ON_BICYCLE"+activity.getConfidence());
                    break;
                }
                case DetectedActivity.TILTING:{
                    Log.d(TAG,"handleDetectedActvty: TILTING"+activity.getConfidence());
                    break;
                }
                case DetectedActivity.UNKNOWN:{
                    Log.d(TAG,"handleDetectedActvty: UNKNOWN"+activity.getConfidence());
                    break;
                }

            }
        }
    }

}
