package com.urbanbees.nextorbis.urbanbeesmobility.store;

public class Products {

    public String provider,name,description,expired,image, valid_until,link,type,category,download_link;
    public int urbees,amount;


    public Products(){
    }

    public Products(int amount,String link,String type, String provider, String name, String description, String expired, String image, int urbees, String valid_until, String category, String download_link) {

        this.amount = amount;
        this.link = link;
        this.description = description;
        this.expired = expired;
        this.image = image;
        this.name = name;
        this.provider = provider;
        this.urbees = urbees;
        this.valid_until = valid_until;
        this.type=type;
      //  this.users=users;
    }


    public int getAmount() {
        return amount;
    }
    public String getType(){return type;}
    public String getDescription() {
        return description;
    }
    public String getExpired() {
        return expired;
    }
    public String getImage() {
        return image;
    }
    public String getName() {
        return name;
    }
    public String getProvider() {
        return provider;
    }
    public int getUrbees() {
        return urbees;
    }
    public String getValid_until() {
        return valid_until;
    }
    public String getLink() {
        return link;
    }
    public String getCategory() {return category;}
    public String getDownload_link(){return download_link;}
  // public String getUsers(){return  users;}

    public void setType(String type){this.type=type;}
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setExpired(String expired) {
        this.expired = expired;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setProvider(String provider) { this.provider = provider; }
    public void setUrbees(int urbees) {
        this.urbees = urbees;
    }
    public void setValid_until(String valid_until) { this.valid_until = valid_until; }
    public void setLink(String link) { this.link = link; }
    public void setCategory(String category){this.category= category;}
    public void setDownload_link(String download_link){this.download_link= download_link;}
    //public void setUsers(String users){this.users= users;}
}
