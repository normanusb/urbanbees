package com.urbanbees.nextorbis.urbanbeesmobility.preapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.urbanbees.nextorbis.urbanbeesmobility.R;
import com.urbanbees.nextorbis.urbanbeesmobility.login.PathActivity;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private static int SLPASH_TIME_OUT = 2500;
    TextView Start_text;
    int number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Start_text=findViewById(R.id.Start_text);

        getSupportActionBar().hide();
        Random random= new Random();
        number= random.nextInt(9 - 1 + 1) + 1;

        switch (number) {
            case 1:
                Start_text.setText(getString(R.string.Start_text));
                break;
            case 2:
                Start_text.setText(getString(R.string.Start_text2));
                break;
            case 3:
                Start_text.setText(getString(R.string.Start_text3));
                break;
            case 4:
                Start_text.setText(getString(R.string.Start_text4));
                break;
            case 5:
                Start_text.setText(getString(R.string.Start_text5));
                break;
            case 6:
                Start_text.setText(getString(R.string.Start_text6));
                break;
            case 7:
                Start_text.setText(getString(R.string.Start_text7));
                break;
            case 8:
                Start_text.setText(getString(R.string.Start_text8));
                break;
            case 9:
                Start_text.setText(getString(R.string.Start_text9));
                break;
            default:
                Start_text.setText(getString(R.string.Start_text));
                break;

        }

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run(){
                Intent homeIntent= new Intent(MainActivity.this, PathActivity.class);
                startActivity(homeIntent);
                finish();

            }

        },SLPASH_TIME_OUT);

    }
}
