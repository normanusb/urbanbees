package com.urbanbees.nextorbis.urbanbeesmobility.profile;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.urbanbees.nextorbis.urbanbeesmobility.R;

public class ClaimCodesActivity extends AppCompatActivity {
    FirebaseAuth auth;
    FirebaseUser user;
    String codetoclaim;
    DatabaseReference reference;
    ImageButton ClaimCodeButton;
    EditText etcode_claim;
    TextView txtinfo_viral_earned,txtName_profile_viral,btviral_text,txtinfo_viral;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claim_codes);

        ClaimCodeButton=  findViewById(R.id.ClaimCodeButton);
        etcode_claim=  findViewById(R.id.etcode_claim);
        txtinfo_viral_earned=  findViewById(R.id.txtinfo_viral_earned);
        txtName_profile_viral=  findViewById(R.id.txtName_profile_viral);
        btviral_text= findViewById(R.id.btviral_text);
        txtinfo_viral=findViewById(R.id.txtinfo_viral);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(txtinfo_viral_earned,70,100,1, TypedValue.COMPLEX_UNIT_PX);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(txtName_profile_viral,70,100,1, TypedValue.COMPLEX_UNIT_PX);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(btviral_text,70,100,1, TypedValue.COMPLEX_UNIT_PX);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(txtinfo_viral,70,100,1, TypedValue.COMPLEX_UNIT_PX);
        ClaimCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClaimCodeAction();
            }
        });



    }
    public void ClaimCodeAction(){
        if(!etcode_claim.getText().toString().equals("")){
            codetoclaim=etcode_claim.getText().toString().trim();

            reference = FirebaseDatabase.getInstance().getReference();

            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.child("codes").child(codetoclaim).exists()){
                        String used = dataSnapshot.child("codes").child(codetoclaim).child("claimed").getValue().toString();
                        if (used.equals("no")) {
                            auth = FirebaseAuth.getInstance();
                            user = auth.getCurrentUser();
                            String user_id=user.getUid();
                            int OldUrbees = Integer.parseInt(dataSnapshot.child("Users").child(user_id).child("urbees").getValue().toString());
                            int newUrbees = OldUrbees + 420;
                            reference.child("Users").child(user_id).child("urbees").setValue(newUrbees);
                            dataSnapshot.child("codes").child(codetoclaim).child("claimed").getRef().setValue("yes");
                            txtinfo_viral_earned.setTextColor(getColor(R.color.positiveGreen));
                            txtinfo_viral_earned.setText(getString(R.string.EarnedbyclaimingCode));
                            etcode_claim.setText("");
                        } else {
                            etcode_claim.setText("");
                            txtinfo_viral_earned.setTextColor(getColor(R.color.colorGoogle));
                            txtinfo_viral_earned.setText(getString(R.string.EarnedbyclaimingCodeUsed));
                        }
                    }else{
                        txtinfo_viral_earned.setTextColor(getColor(R.color.colorGoogle));
                        txtinfo_viral_earned.setText(getString(R.string.EarnedbyclaimingCodeWrong));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(ClaimCodesActivity.this, databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });

        }

    }
}
