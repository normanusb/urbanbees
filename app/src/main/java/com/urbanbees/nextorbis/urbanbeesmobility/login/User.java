package com.urbanbees.nextorbis.urbanbeesmobility.login;

public class User {
    public String useremail,name,firsttime, firsttimechallenges,firsttimeplaces, firsttimestore, firsttimenews ;
    public int urbees, turisticttimes, mobilitytimes,eventtimes;

    public User(String useremaill, String names,String firsttime,String firsttimechallenges,String firsttimeplaces,String firsttimestore, String firsttimenews,int urbeess,int turisticttimess, int mobilitytimess, int eventtimess){

        this.useremail= useremaill;
        this.name= names;
        this.urbees=urbeess;
        this.turisticttimes=turisticttimess;
        this.mobilitytimes=mobilitytimess;
        this.eventtimes=eventtimess;
        this.firsttime=firsttime;
        this.firsttimechallenges=firsttimechallenges;
        this.firsttimeplaces=firsttimeplaces;
        this.firsttimestore=firsttimestore;
        this.firsttimenews= firsttimenews;
    }

    public int getMobilitytimes() {
        return mobilitytimes;
    }
    public int getEventtimes() {
        return eventtimes;
    }
    public int getTuristicttimes() {
        return turisticttimes;
    }
    public String getFirsttimenews(){ return firsttimenews; }
    public String getFirsttime (){ return firsttime; }
    public String getFirsttimechallenges (){ return firsttimechallenges; }
    public String getFirsttimeplaces (){ return firsttimeplaces; }
    public String getFirsttimestore (){ return firsttimestore; }

    public int getUrbees() {
        return urbees;
    }

    public String getName() {
        return name;
    }

    public String getUseremail() {
        return useremail;
    }
}
