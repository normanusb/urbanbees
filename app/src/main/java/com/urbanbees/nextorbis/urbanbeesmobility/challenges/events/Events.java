package com.urbanbees.nextorbis.urbanbeesmobility.challenges.events;

public class Events {
    public String address,date,link,name,beginen,description,end,expired,image,code, reservation_from;
    public double lat,lon;
    public int numberofusers,urbees;

    public Events(){
    }

    public Events(String reservation_from, String code, String address,double lat, double lon, String date, String link, String name, String beginen, String description, String end, String expired, String image, int numberofusers, int urbees) {
        this.address = address;
        this.date = date;
        this.link = link;
        this.name = name;
        this.beginen = beginen;
        this.description = description;
        this.end = end;
        this.expired = expired;
        this.image = image;
        this.numberofusers = numberofusers;
        this.urbees = urbees;
        this.lon= lon;
        this.lat=lat;
        this.code=code;
        this.reservation_from=reservation_from;
    }

    public String getAddress() {
        return address;
    }
    public double getLat() {
        return lat;
    }
    public double getLon() {
        return lon;
    }
    public String getCode(){return code;}
    public String getDate() {
        return date;
    }
    public String getLink() {
        return link;
    }
    public String getName() {
        return name;
    }
    public String getBeginen() {
        return beginen;
    }
    public String getDescription() {
        return description;
    }
    public String getEnd() {
        return end;
    }
    public String getExpired() {
        return expired;
    }
    public String getImage() {
        return image;
    }
    public String getReservation_from() {
        return reservation_from;
    }
    public int getNumberofusers() {
        return numberofusers;
    }
    public int getUrbees() {
        return urbees;
    }


    public void setAddress(String address) {
        this.address = address;
    }
    public void setLat(double lat) {
        this.lat = lat;
    }
    public void setLon(double lon) {
        this.lon = lon;
    }
    public void setCode(String code){this.code=code;}
    public void setDate(String date) {
        this.date = date;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setReservation_from(String reservation_from){
        this.reservation_from=reservation_from;
    }
    public void setBeginen(String beginen) {
        this.beginen = beginen;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setEnd(String end) {
        this.end = end;
    }
    public void setExpired(String expired) {
        this.expired = expired;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public void setNumberofusers(int numberofusers) {
        this.numberofusers = numberofusers;
    }
    public void setUrbees(int urbees) {
        this.urbees = urbees;
    }
}
