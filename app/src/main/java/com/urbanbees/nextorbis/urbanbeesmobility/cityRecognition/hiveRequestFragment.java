package com.urbanbees.nextorbis.urbanbeesmobility.cityRecognition;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.urbanbees.nextorbis.urbanbeesmobility.R;


public class hiveRequestFragment extends Fragment {

    ImageButton request_Hive;
    TextView apologies_text,request_Hive_text, apologies_text_top;
    String city_name,user_id;
    FirebaseAuth auth;
    FirebaseUser user;
    DatabaseReference reference;
    Activity activity1;
    Boolean nolocation;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hive_request, container, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState) {

        activity1 = getActivity();

        if (isAdded() && activity1 != null) {
                request_Hive = view.findViewById(R.id.btRequestHive);
                request_Hive_text = view.findViewById(R.id.btRequestHive_text);
                apologies_text = view.findViewById(R.id.apologiesInstructionText);
                apologies_text_top=view.findViewById(R.id.apologiestext);

                 nolocation = getArguments().getBoolean("nolocation");
                //user id for city request
                auth = FirebaseAuth.getInstance();
                user = auth.getCurrentUser();
                user_id = user.getUid();

                if(!nolocation) {

                    request_Hive_text.setText(getString(R.string.requestHive));
                    apologies_text.setText(getString(R.string.instructionApologies));
                    apologies_text_top.setText(getString(R.string.apologies));
                    //city name for city request
                    SharedPreferences sharedPreferences = activity1.getSharedPreferences("city", activity1.MODE_PRIVATE);
                    city_name = sharedPreferences.getString("city", "");
                    reference = FirebaseDatabase.getInstance().getReference().child("Cities").child(city_name).child("Users_requesting");
                    reference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (isAdded() && activity1 != null) {
                                DataSnapshot snapshot = dataSnapshot.child(user_id);
                                if (snapshot.exists()) {
                                    request_Hive.setEnabled(false);
                                    request_Hive_text.setText(getString(R.string.request_city));
                                    apologies_text.setText(getString(R.string.thanks_request_city));
                                } else {
                                    request_Hive.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            reference.child(user_id).child("request").setValue("yes");
                                            Toast.makeText(activity1,
                                                    "Your request has been sent, we are looking forward to making this city part of our hive ",
                                                    Toast.LENGTH_SHORT)
                                                    .show();
                                            request_Hive.setEnabled(false);
                                            request_Hive_text.setText(getString(R.string.request_city));
                                            apologies_text.setText(getString(R.string.thanks_request_city));
                                        }

                                    });

                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Toast.makeText(activity1, databaseError.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }else{
                    request_Hive.setVisibility(View.GONE);
                    request_Hive_text.setVisibility(View.GONE);

                }

        }
    }
}