package com.urbanbees.nextorbis.urbanbeesmobility.store;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.urbanbees.nextorbis.urbanbeesmobility.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import dmax.dialog.SpotsDialog;

public class InfoActivity extends AppCompatActivity {

    private static final int PERMISION_REQUEST_CODE =1000 ;
    ImageButton button_go, button_moreInfo,dialog_button,dialogyes_button,dialogno_button;
    ImageView imageProduct;
    int deviceWidth, deviceHeight;
    String type,product_link;
    DatabaseReference reference;
    DatabaseReference reference2;
    FirebaseAuth auth;
    FirebaseUser user;
    boolean after_date= false;
    TextView name_txt, info_txt, info2_txt,button_product_buy_text, button_product_info_text,button_product_buy_text_move,dialoginformation, dialogtitle,dialogyesnotitle,dialogyesnoinformation;
    String productId,product_name, product_description,
            product_urbees,product_provider,product_image,
            productType,activebutton,Product_link_download,product_amout, product_time,
            //city information
            city_name,product_city,
           //User info
            user_id;

    DataSnapshot mainSnapshot;
    //for product download
    Dialog welcomingDialog1, sureDialog;
//for image downloading
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PERMISION_REQUEST_CODE:
            {
                if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //Alert dialog for permision granted
                    Okdialog(getString(R.string.granted),getString(R.string.permisiongranted_go_geturbees),false,false,false);
                 }
                else{
                    Toast.makeText(InfoActivity.this, "Permission denied",Toast.LENGTH_LONG).show();
            }
            }
            break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        getSupportActionBar().hide();

        //Determine width and height of the screen for the images from firebase
       widhandheight thread = new widhandheight();
        thread.start();


        SharedPreferences sharedPreferences = getSharedPreferences("city", MODE_PRIVATE);
        city_name= sharedPreferences.getString("city","");
        //items from Veiw
        imageProduct= findViewById(R.id.product_image_info);
        name_txt= findViewById(R.id.txtName_info);
        TextViewCompat.setAutoSizeTextTypeWithDefaults(name_txt,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        info_txt= findViewById(R.id.txtInfo_info);
         TextViewCompat.setAutoSizeTextTypeWithDefaults(info_txt,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        info2_txt= findViewById(R.id.txtinfo_info2);
        TextViewCompat.setAutoSizeTextTypeWithDefaults(info2_txt,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
       // TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(info2_txt,25,50,1, TypedValue.COMPLEX_UNIT_PX);

        button_moreInfo= findViewById(R.id.moreinfo_product);
        button_go= findViewById(R.id.go_info);
        button_product_buy_text= findViewById(R.id.button_product_buy_text);
        button_product_info_text= findViewById(R.id.button_product_info_text);
        button_product_buy_text_move= findViewById(R.id.button_product_buy_text_move);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(button_product_buy_text,25,50,1, TypedValue.COMPLEX_UNIT_PX);

        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(button_product_info_text,15,50,1, TypedValue.COMPLEX_UNIT_PX);
       // TextViewCompat.setAutoSizeTextTypeWithDefaults(button_product_info_text,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        //business logic variables
        type= getIntent().getStringExtra("type");
        activebutton=getIntent().getStringExtra("active");
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        user_id=user.getUid();

        if(type.equals("product")){

            reference= FirebaseDatabase.getInstance().getReference().child("Cities").child(city_name).child("Products");
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    productId= getIntent().getStringExtra("productID");
                    mainSnapshot=dataSnapshot.child(productId);
                    productType= mainSnapshot.child("type").getValue().toString();
                    product_name= mainSnapshot.child("name").getValue().toString();
                    product_description= mainSnapshot.child("description").getValue().toString();
                    product_link= mainSnapshot.child("link").getValue().toString();
                    product_urbees= mainSnapshot.child("urbees").getValue().toString();
                    product_provider= mainSnapshot.child("provider").getValue().toString();
                    product_image= mainSnapshot.child("image").getValue().toString();
                    Product_link_download=mainSnapshot.child("download_link").getValue().toString();
                    product_amout=mainSnapshot.child("amount").getValue().toString();
                    product_time=mainSnapshot.child("valid_until").getValue().toString();

                    name_txt.setText(product_name);

                    String producttypetext="";
                    if(productType.equals("send")){
                         producttypetext=getString(R.string.producttypetext_send);
                    }else if(productType.equals("download")){
                        producttypetext=getString(R.string.producttypetext_download);
                    }else if(productType.equals("link")){
                        producttypetext=getString(R.string.producttypetext_link);
                    }else if(productType.equals("claim")){
                        producttypetext=getString(R.string.producttypetext_claim);
                    }else if(productType.equals("raffle")){
                        producttypetext=getString(R.string.producttypetext_raffle);
                    }

                    String infotexttoset= product_description+System.getProperty("line.separator")+getString(R.string.provider_text)+" "+ product_provider+System.getProperty("line.separator")+producttypetext+"\r\n";
                    String infotexttoset2=getString(R.string.product_cost_text)+" "+product_urbees+" ";
                    if(productType.equals("raffle")){
                        button_product_buy_text.setText("Raffle");
                        button_product_info_text.setText("Buy");
                        infotexttoset2+=getString(R.string.raffle_discount_text);
                        infotexttoset+=producttypetext+System.getProperty("line.separator")+getString(R.string.raffle_time)+" "+product_time;
                        if( mainSnapshot.child("users").child(user_id).child("raffle").exists() && Integer.parseInt(mainSnapshot.child("users").child(user_id).child("raffle").getValue().toString())>0){
                            infotexttoset+="\r\n"+"You played this raffle "+mainSnapshot.child("users").child(user_id).child("raffle").getValue().toString() +" times already";
                        }
                        try{
                            after_date=  new SimpleDateFormat("dd.MM.yyyy").parse(product_time).before(new Date());
                        }catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if(after_date) {
                            activebutton="yes";
                            button_go.setEnabled(true);
                            button_product_buy_text.setText(getString(R.string.seeRaffleWinner));
                        }
                    }
                    info_txt.setText(infotexttoset);
                    info2_txt.setText(infotexttoset2);
                    //Picasso.get().load(product_image).placeholder(R.drawable.chargepage).resize(deviceWidth*3, deviceHeight/2).into(imageProduct);
                    Picasso.get().load(product_image).placeholder(R.drawable.chargepage).into(imageProduct);
                    button_moreInfo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent browsermaps= new Intent (Intent.ACTION_VIEW, Uri.parse(product_link));
                            startActivity(browsermaps);
                        }
                    });


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();


                }
            });
            if( activebutton.equals("no")) {
                button_go.setVisibility(View.GONE);
                button_product_buy_text_move.setVisibility(View.GONE);
                button_product_buy_text.setTextColor(getColor(R.color.colorBlack));
                button_product_buy_text.setText(getString(R.string.not_enough_urbees));

            }
                button_go.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Integer.parseInt(product_amout)>0){

                         if(productType.equals("send")){
                             Intent goSendpage = new Intent(InfoActivity.this, SendProductActivity.class);
                             goSendpage.putExtra("productID", productId);
                             finish();
                             startActivity(goSendpage);

                         }else if(productType.equals("download")){
                             //download the image


                                setSureDialog(getString(R.string.buy_product_download_sure_title),getString(R.string.buy_product_download_sure_text),1);



                         }else if(productType.equals("link")){
                             setSureDialog(getString(R.string.buy_product_download_sure_title),getString(R.string.buy_product_download_sure_text),5);

                         } else if(productType.equals("claim")){
                            setSureDialog(getString(R.string.buy_product_download_sure_title),getString(R.string.buy_product_download_sure_text),2);

                        }else if(productType.equals("raffle")){

                             if(after_date) {

                                //ended raffle
                                Okdialog(getString(R.string.warining),getString(R.string.raffleTextEarn),false,true,false);

                             }else{
                                 //raffle on
                                 setSureDialog(getString(R.string.raffleforproduct),getString(R.string.raffle_product_sure_text),4);
                             }
                         }else{
                             Intent browsermaps= new Intent (Intent.ACTION_VIEW, Uri.parse("https://nextorbis.com/urbanbees/"));
                             startActivity(browsermaps);
                        }
                    }else{
                        reference2.child("Cities").child(city_name).child("Products").child(productId).child("expired").setValue("yes");
                        //not available
                        Okdialog(getString(R.string.Product_Unavailable),getString(R.string.product_not_available),true,false,false);

                    }


                }
            });

        }else if(type.equals("productToClaim")){
            productId= getIntent().getStringExtra("productID");
            product_city= getIntent().getStringExtra("cityProduct");


            reference= FirebaseDatabase.getInstance().getReference().child("Cities").child(product_city).child("Products");
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    mainSnapshot=dataSnapshot.child(productId);
                    productType= mainSnapshot.child("type").getValue().toString();
                    product_name= mainSnapshot.child("name").getValue().toString();
                    product_description= mainSnapshot.child("description").getValue().toString();
                    product_link= mainSnapshot.child("address").getValue().toString();
                    product_urbees= mainSnapshot.child("urbees").getValue().toString();
                    product_provider= mainSnapshot.child("provider").getValue().toString();
                    product_image= mainSnapshot.child("image").getValue().toString();
                    Product_link_download=mainSnapshot.child("download_link").getValue().toString();
                    product_amout=mainSnapshot.child("amount").getValue().toString();

                    name_txt.setText(product_name);
                    info_txt.setText(product_description+System.getProperty("line.separator")+getString(R.string.provider_text)+" "+ product_provider+System.getProperty("line.separator")+ getString(R.string.claimWarning) );
                    info2_txt.setText(getString(R.string.product_cost_text)+" "+product_urbees);
                    button_product_info_text.setText(getString(R.string.adressToClaim));
                    button_product_buy_text.setText(getString(R.string.claimUrbees));

                    Picasso.get().load(product_image).resize(deviceWidth*3, deviceHeight/2).placeholder(R.drawable.chargepage).into(imageProduct);

                    button_moreInfo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent browsermaps= new Intent (Intent.ACTION_VIEW, Uri.parse(product_link));
                            startActivity(browsermaps);
                        }
                    });

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();


                }

        });
            button_go.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // claim the product.
                    setSureDialog(getString(R.string.warining),getString(R.string.claim_product_sure_text),3);
                }
            });

    }

}
    private void setNewUrbeesAndProductAmount() {


        reference2= FirebaseDatabase.getInstance().getReference();
        reference2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int urbeesUser = Integer.parseInt(dataSnapshot.child("Users").child(user_id).child("urbees").getValue().toString());
                int newUrbeesUser = urbeesUser - Integer.parseInt(product_urbees);
                reference2.child("Users").child(user_id).child("urbees").setValue(newUrbeesUser);

                reference2.child("Cities").child(city_name).child("Products").child(productId).child("users").child(user_id).child("bought").setValue("yes");
                int amountProduct= Integer.parseInt(product_amout);
                int newamountProduct= amountProduct-1;
                reference2.child("Cities").child(city_name).child("Products").child(productId).child("amount").setValue(newamountProduct);

                if(dataSnapshot.child("Cities").child(city_name).child("Products").child(productId).child("users").child(user_id).child("times").exists()){
                    int timesUserBougthItem= Integer.parseInt(dataSnapshot.child("Cities").child(city_name).child("Products").child(productId).child("users").child(user_id).child("times").getValue().toString());
                    int newTimesUserBougthItem= timesUserBougthItem+1;
                    reference2.child("Cities").child(city_name).child("Products").child(productId).child("users").child(user_id).child("times").setValue(newTimesUserBougthItem);
                }else{
                    reference2.child("Cities").child(city_name).child("Products").child(productId).child("users").child(user_id).child("times").setValue(1);

                }
                if(newamountProduct==0){
                    reference2.child("Cities").child(city_name).child("Products").child(productId).child("expired").setValue("yes");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
    }

    private void claimfunctionality(){
        button_go.setEnabled(false);
        reference2= FirebaseDatabase.getInstance().getReference();
        reference2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

               if( dataSnapshot.child("Users").child(user_id).child("products").child(productId).child("claimed").exists()){
                   int amount_claimed= Integer.parseInt(dataSnapshot.child("Users").child(user_id).child("products").child(productId).child("claimed").getValue().toString());
                   reference2.child("Users").child(user_id).child("products").child(productId).child("claimed").setValue(amount_claimed+1);
               }else{
                   reference2.child("Users").child(user_id).child("products").child(productId).child("claimed").setValue(1);
                   reference2.child("Users").child(user_id).child("products").child(productId).child("city").setValue(city_name);
                   reference2.child("Users").child(user_id).child("products").child(productId).child("name").setValue(product_name);
               }
                Okdialog(getString(R.string.plain_congratulations),getString(R.string.congrats_claim),false,false,false);
                button_go.setEnabled(true);
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();
                button_go.setEnabled(true);
            }
        });

    }
    private void   productClaiminplace(){
        button_go.setEnabled(false);
        reference2= FirebaseDatabase.getInstance().getReference();
        reference2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    int amount_claimed= Integer.parseInt(dataSnapshot.child("Users").child(user_id).child("products").child(productId).child("claimed").getValue().toString());
                    if(amount_claimed>0) {
                        reference2.child("Users").child(user_id).child("products").child(productId).child("claimed").setValue(amount_claimed - 1);

                        Okdialog(getString(R.string.plain_congratulations),getString(R.string.congrats_claimed_now),true,false,false);
                        button_go.setEnabled(true);
                    }else{
                        Toast.makeText(getApplicationContext(), getString(R.string.no_products_to_claim), Toast.LENGTH_LONG).show();
                        button_go.setEnabled(true);
                        finish();
                    }

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();
                button_go.setEnabled(true);
            }
        });


    }
    private void   productraffle(){
        reference2= FirebaseDatabase.getInstance().getReference();
        reference2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int rafflevalue=0;
               if (dataSnapshot.child("Cities").child(city_name).child("Products").child(productId).child("users").child(user_id).child("raffle").exists()){
                    rafflevalue= Integer.parseInt(dataSnapshot.child("Cities").child(city_name).child("Products").child(productId).child("users").child(user_id).child("raffle").getValue().toString())+1;
               }else{
                   rafflevalue=1;
               }
                reference2.child("Cities").child(city_name).child("Products").child(productId).child("users").child(user_id).child("raffle").setValue(rafflevalue);

                int urbeesUser = Integer.parseInt(dataSnapshot.child("Users").child(user_id).child("urbees").getValue().toString());
                int newUrbeesUser = urbeesUser - Integer.parseInt(product_urbees);
                reference2.child("Users").child(user_id).child("urbees").setValue(newUrbeesUser);
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();
            }
        });


    }
    private void rafflewinneralgorithm(){
        reference2= FirebaseDatabase.getInstance().getReference();
        reference2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String winnerofraffle="none";
                List<String> raffleparticipants = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.child("Cities").child(city_name).child("Products").child(productId).child("users").getChildren()) {
                    String useridraffle=snapshot.getKey().toString();
                    if(snapshot.child("raffle").exists() && Integer.parseInt(snapshot.child("raffle").getValue().toString())>0){

                        for(int i=0;i<Integer.parseInt(snapshot.child("raffle").getValue().toString()); i++) {
                            raffleparticipants.add(useridraffle);
                        }
                        snapshot.child("raffle").getRef().setValue(0);
                    }else if (Integer.parseInt(snapshot.child("raffle").getValue().toString())==-1){
                        winnerofraffle= useridraffle;
                    }
                }
                if(winnerofraffle.equals("none")) {
                    if(raffleparticipants.size()>0) {
                        final int min = 0;
                        final int max = raffleparticipants.size() - 1;
                        final int random = new Random().nextInt((max - min) + 1) + min;
                        winnerofraffle = raffleparticipants.get(random);
                        reference2.child("Cities").child(city_name).child("Products").child(productId).child("users").child(winnerofraffle).child("bought").setValue("yes");
                        reference2.child("Cities").child(city_name).child("Products").child(productId).child("users").child(winnerofraffle).child("raffle").setValue(-1);

                        if (dataSnapshot.child("Cities").child(city_name).child("Products").child(productId).child("users").child(winnerofraffle).child("times").exists()) {
                            int timesUserBougthItem = Integer.parseInt(dataSnapshot.child("Cities").child(city_name).child("Products").child(productId).child("users").child(winnerofraffle).child("times").getValue().toString());
                            int newTimesUserBougthItem = timesUserBougthItem + 1;
                            reference2.child("Cities").child(city_name).child("Products").child(productId).child("users").child(winnerofraffle).child("times").setValue(newTimesUserBougthItem);
                        } else {
                            reference2.child("Cities").child(city_name).child("Products").child(productId).child("users").child(winnerofraffle).child("times").setValue(1);
                        }
                    }
                }
                if(winnerofraffle.equals(user_id)){
                    reference2.child("Cities").child(city_name).child("Products").child(productId).child("expired").setValue("yes");
                    Okdialog(getString(R.string.youwintitle),getString(R.string.youwintext),false,false,true);

                }else if(winnerofraffle.equals("none") && raffleparticipants.size()==0){
                    reference2.child("Cities").child(city_name).child("Products").child(productId).child("expired").setValue("yes");
                    Okdialog(getString(R.string.tryagain),getString(R.string.youloosetext),true,false,false);

                }else{
                    Okdialog(getString(R.string.tryagain),getString(R.string.youloosetext),true,false,false);
                }


            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }
    private void openlinkproduct() {
        reference2= FirebaseDatabase.getInstance().getReference();
        reference2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String link= dataSnapshot.child("Cities").child(city_name).child("Products").child(productId).child("download_link").getValue().toString();
                finish();
                Intent browsermaps2= new Intent (Intent.ACTION_VIEW, Uri.parse(link));
                startActivity(browsermaps2);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }
    //Determine width and height of the screen for the images from firebase
    class widhandheight extends Thread {

        @Override
        public void run() {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager windowmanager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
            deviceWidth = Math.round(displayMetrics.widthPixels/3);
            deviceHeight= displayMetrics.heightPixels;

        }
    }
    public void Okdialog(String title, String information, final boolean cancel, final boolean raffle, final boolean win){
        welcomingDialog1= new Dialog(InfoActivity.this);
        welcomingDialog1.setContentView(R.layout.popup_notification);

        dialoginformation = welcomingDialog1.findViewById(R.id.text_description);
        dialogtitle = welcomingDialog1.findViewById(R.id.information_text);
        dialog_button= welcomingDialog1.findViewById(R.id.button_ok);

        dialogtitle.setText(title);
        dialoginformation.setText(information);

        dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                welcomingDialog1.dismiss();
                if(raffle){
                    rafflewinneralgorithm();
                }
                if(win){
                    openlinkproduct();
                }
                if(cancel){
                    finish();
                }

            }
        });
        welcomingDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        welcomingDialog1.show();
    }
    public void setSureDialog(String title, String information, final int type){
        sureDialog= new Dialog(InfoActivity.this);
        sureDialog.setContentView(R.layout.popup_yesorno);

        dialogyesnoinformation = sureDialog.findViewById(R.id.text_description_2);
        dialogyesnotitle = sureDialog.findViewById(R.id.information_text_2);
        dialogyes_button= sureDialog.findViewById(R.id.buttonyes);
        dialogno_button= sureDialog.findViewById(R.id.buttonno);

        dialogyesnotitle.setText(title);
        dialogyesnoinformation.setText(information);

        dialogyes_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (type) {
                    case 1:
                        setNewUrbeesAndProductAmount();
                        if(ActivityCompat.checkSelfPermission(InfoActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ){
                            requestPermissions(new String[]{
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE

                            },PERMISION_REQUEST_CODE);
                            return;
                        }else{
                            AlertDialog dialog8 = new SpotsDialog(InfoActivity.this);
                            dialog8.show();
                            dialog8.setMessage(getString(R.string.downloading_image_text));
                            String filename= UUID.randomUUID().toString()+".jpg";
                            Picasso.get()
                                    .load(Product_link_download)
                                    .into(new SaveImageHelper(InfoActivity.this,
                                            dialog8,
                                            getApplicationContext().getContentResolver(),
                                            filename,
                                            "UrbanBees awesome poster"));
                        }
                        break;
                    case 2:
                        setNewUrbeesAndProductAmount();
                        claimfunctionality();
                        break;
                    case 3:
                        productClaiminplace();
                        break;
                    case 4:
                        productraffle();
                        finish();
                        break;
                    default:
                        break;
                }
                sureDialog.dismiss();
                if(type==5){
                    Intent browsermaps= new Intent (Intent.ACTION_VIEW, Uri.parse(Product_link_download));
                    startActivity(browsermaps);
                    setNewUrbeesAndProductAmount();
                }
            }
        });
        dialogno_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sureDialog.dismiss();
            }
        });
        sureDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        sureDialog.show();

    }

}
