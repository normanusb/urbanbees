package com.urbanbees.nextorbis.urbanbeesmobility.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.urbanbees.nextorbis.urbanbeesmobility.R;
import com.urbanbees.nextorbis.urbanbeesmobility.login.PathActivity;
import com.urbanbees.nextorbis.urbanbeesmobility.profile.products.prodcutsToClaimActivity;
import com.urbanbees.nextorbis.urbanbeesmobility.profile.viralmarketing.ViralActivity;

import java.util.Locale;


public class ProfileFragment extends Fragment {

    FirebaseAuth auth;
    FirebaseUser user;
    TextView profileTxt, txt_urbees, txt_eventsamount, two_info_profileamount,
            txt_turisticamount,txt_totalMove_urbees_intro, two_info_profile_text,txt_events,
            txt_turistic, four_info_profile,four_txt_lernamount,btproducts_claim_text,btviral_text,btlogout_text;
    String user_id;
    DatabaseReference reference;
    ProgressBar spinner;
    ImageButton logoutbutton, btproducts, btviral, btlenguage, btcodes ;
    ImageView urbees_image,two_profile_image, one_profile_image,three_profile_image, four_profile_image;
    Activity activity1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState){

        activity1 = getActivity();

        if (isAdded() && activity1 != null) {
            logoutbutton = view.findViewById(R.id.btlogout);
            btlenguage = view.findViewById(R.id.btlenguage);
            btproducts = view.findViewById(R.id.btproducts);
            btcodes= view.findViewById(R.id.btcodes);
            btviral= view.findViewById(R.id.btviral);
            spinner = view.findViewById(R.id.progressBarprofile);
            auth = FirebaseAuth.getInstance();
            profileTxt = view.findViewById(R.id.txtName_profile);
            txt_urbees = view.findViewById(R.id.txt_urbees);
            txt_eventsamount = view.findViewById(R.id.txt_eventsamount);
            two_info_profileamount = view.findViewById(R.id.two_info_profileamount);
            two_info_profile_text= view.findViewById(R.id.two_info_profile);
            txt_events=view.findViewById(R.id.txt_events);
            txt_turistic= view.findViewById(R.id.txt_turistic);
            txt_turisticamount = view.findViewById(R.id.txt_turisticamount);
            txt_totalMove_urbees_intro= view.findViewById(R.id.txt_totalMove_urbees_intro);
            four_info_profile= view.findViewById(R.id.four_info_profile);
            four_txt_lernamount= view.findViewById(R.id.four_txt_lernamount);
           /* txt_profileamountsmall=view.findViewById(R.id.txt_profileamountsmall);
            txt_eventsamountsmall=view.findViewById(R.id.txt_eventsamountsmall);
            txt_turisticamountsmall=view.findViewById(R.id.txt_turisticamountsmall);*/
            two_profile_image=view.findViewById(R.id.two_profile_image);
            one_profile_image=view.findViewById(R.id.one_profile_image);
            three_profile_image=view.findViewById(R.id.three_profile_image);
            four_profile_image=view.findViewById(R.id.four_profile_image);
            urbees_image= view.findViewById(R.id.urbees_image);
            btproducts_claim_text= view.findViewById(R.id.btproducts_claim_text);
            btviral_text= view.findViewById(R.id.btviral_text);
            btlogout_text= view.findViewById(R.id.btlogout_text);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(profileTxt,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(txt_urbees,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(txt_totalMove_urbees_intro,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(btproducts_claim_text,25,50,1, TypedValue.COMPLEX_UNIT_PX);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(two_info_profile_text,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(two_info_profileamount,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
           // TextViewCompat.setAutoSizeTextTypeWithDefaults(txt_profileamountsmall,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(txt_events,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
           // TextViewCompat.setAutoSizeTextTypeWithDefaults(txt_eventsamountsmall,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(txt_eventsamount,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(txt_turisticamount,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            //TextViewCompat.setAutoSizeTextTypeWithDefaults(txt_turisticamountsmall,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(four_info_profile,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(four_txt_lernamount,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(txt_turistic,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(btviral_text,25,50,1, TypedValue.COMPLEX_UNIT_PX);
            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(btlogout_text,25,50,1, TypedValue.COMPLEX_UNIT_PX);

            two_profile_image.setVisibility(View.GONE);
            one_profile_image.setVisibility(View.GONE);
            three_profile_image.setVisibility(View.GONE);
            four_profile_image.setVisibility(View.GONE);
            urbees_image.setVisibility(View.GONE);
            btproducts.setEnabled(false);
            logoutbutton.setEnabled(false);
            user = auth.getCurrentUser();
            user_id = user.getUid();

             //lenguage functionality
            btlenguage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Locale current = getResources().getConfiguration().locale;
                    if(current.getLanguage().toLowerCase().equals("en")){
                        setLocale("de");
                    }else{
                        setLocale("en");
                    }
                }
            });

            //logout functionality
            logoutbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signOut();

                }
            });
            btproducts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    productsPage();
                }
            });
            btviral.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Viralmarketingpage();
                }
            });
            btcodes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToClaimPoints();
                }
            });

            //Code to retrieve data from the Database
            //make a reference poiting to a specific user ( the one that is logged in).
            reference = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id);
            // reference2= FirebaseDatabase.getInstance().getReference().child("Events");


            spinner.setVisibility(View.VISIBLE);
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String urbees = dataSnapshot.child("urbees").getValue().toString();
                    String eventamount = dataSnapshot.child("eventtimes").getValue().toString();
                    String kmamount = dataSnapshot.child("mobilitytimes").getValue().toString();
                    String turisticamount = dataSnapshot.child("turisticttimes").getValue().toString();
                    String learningtimes= "0";
                    if(dataSnapshot.child("learningtimes").exists()){
                        learningtimes= dataSnapshot.child("learningtimes").getValue().toString();
                    }
                    profileTxt.setText(user.getDisplayName());
                    txt_urbees.setText(urbees);
                    txt_eventsamount.setText(eventamount);
                    two_info_profileamount.setText(kmamount);
                    four_txt_lernamount.setText(learningtimes);
                    txt_turisticamount.setText(turisticamount);
                    txt_totalMove_urbees_intro.setText(R.string.totalurbeestext);
                    two_info_profile_text.setText(R.string.FitBee);
                    txt_events.setText(R.string.polinizerBee);
                    /*txt_profileamountsmall.setText(R.string.times);
                    txt_eventsamountsmall.setText(R.string.eventschallenges);
                    txt_turistic.setText(R.string.explored);
                    txt_turisticamountsmall.setText(R.string.placesprofile);*/
                    urbees_image.setVisibility(View.VISIBLE);
                    btproducts.setEnabled(true);
                    logoutbutton.setEnabled(true);
                    btviral.setEnabled(true);

                    int amountFit= Integer.parseInt(kmamount);
                    int amountTurism= Integer.parseInt(turisticamount);
                    int amountLearnings= Integer.parseInt(learningtimes);
                    int amoutevents= Integer.parseInt(eventamount);

                    if (amountFit >200 && amountFit<1000) {
                        two_profile_image.setImageResource(R.drawable.bee4);
                    }else if(amountFit>=1000){
                        two_profile_image.setImageResource(R.drawable.bee2);
                    }

                    if (amountTurism >20 && amountTurism<50) {
                        three_profile_image.setImageResource(R.drawable.bee3);
                    }else if(amountTurism>=30){
                        three_profile_image.setImageResource(R.drawable.bee5);
                    }

                    if (amountLearnings >10 && amountLearnings<50) {
                        four_profile_image.setImageResource(R.drawable.bee4);
                    }else if(amountLearnings>=30){
                        four_profile_image.setImageResource(R.drawable.bee2);
                    }

                    if (amoutevents >10 && amoutevents<30) {
                        one_profile_image.setImageResource(R.drawable.bee3);
                    }else if(amoutevents>=30){
                        one_profile_image.setImageResource(R.drawable.bee5);
                    }



                    two_profile_image.setVisibility(View.VISIBLE);
                    one_profile_image.setVisibility(View.VISIBLE);
                    three_profile_image.setVisibility(View.VISIBLE);
                    four_profile_image.setVisibility(View.VISIBLE);
                    spinner.setVisibility(View.GONE);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(activity1, databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });

        }
    }

    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        if(Build.VERSION.SDK_INT >=Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(locale);
        }else {
            config.locale = locale;
        }
        getActivity().getBaseContext().getResources().updateConfiguration(config,getActivity().getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor= getActivity().getSharedPreferences("settings", Context.MODE_PRIVATE).edit();
        editor.putString("language",lang);
        editor.apply();
        getActivity().recreate();
    }


    public void signOut(){

        if (isAdded() && activity1 != null) {
            auth.signOut();
            LoginManager.getInstance().logOut();

            activity1.finish();
            Intent i = new Intent(activity1, PathActivity.class);
            startActivity(i);
        }
    }

    public void productsPage(){

        if (isAdded() && activity1 != null) {
            Intent i2 = new Intent(activity1, prodcutsToClaimActivity.class);
            startActivity(i2);
        }

    }
    public void Viralmarketingpage(){
        if (isAdded() && activity1 != null) {
            Intent i3 = new Intent(activity1, ViralActivity.class);
            startActivity(i3);
        }

    }
    //TODO add new code claiming activity
    public void goToClaimPoints(){
        if (isAdded() && activity1 != null) {
            Intent i4 = new Intent(activity1, ClaimCodesActivity.class);
            startActivity(i4);
        }

    }


    }


