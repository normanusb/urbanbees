package com.urbanbees.nextorbis.urbanbeesmobility.main;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.location.ActivityTransitionRequest;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.urbanbees.nextorbis.urbanbeesmobility.R;
import com.urbanbees.nextorbis.urbanbeesmobility.challenges.NewsFragment;
import com.urbanbees.nextorbis.urbanbeesmobility.challenges.events.EventsFragment;
import com.urbanbees.nextorbis.urbanbeesmobility.challenges.places.TourismFragment;
import com.urbanbees.nextorbis.urbanbeesmobility.cityRecognition.ActivityRecognizedService;
import com.urbanbees.nextorbis.urbanbeesmobility.cityRecognition.hiveRequestFragment;
import com.urbanbees.nextorbis.urbanbeesmobility.dopamine.dopamineFragment;
import com.urbanbees.nextorbis.urbanbeesmobility.profile.ProfileFragment;
import com.urbanbees.nextorbis.urbanbeesmobility.store.HoneyFragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AppActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private BottomNavigationView mainNav;
    private BottomNavigationView mainNavTop;
    //for Location of the user for the city recognition
    private FusedLocationProviderClient locationclient;
    //first fragment to open in the app Hive ( where the user sees his points and collects for walking and riding a bike)
    private hiveRequestFragment HiveRequestFragment;
    private dopamineFragment dopaminefragment;
    private HoneyFragment honeyFragment;
    private TourismFragment tourismFragment;
    private EventsFragment eventsFragment;
    private ProfileFragment profileFragment;
    private NewsFragment newsFragment;
    public GoogleApiClient mApiClient;
    ActivityRecognitionClient activityRecognitionClient;
    PendingIntent pendingIntent;
    String city_name;
    Boolean city_is_beehive=false, nolocation=false,eventactive=false, storeactive=false, placesactive=false;
    ArrayList<String> cities_available = new ArrayList<String>();
    FirebaseAuth auth;
    DatabaseReference cityRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_app);
        getSupportActionBar().hide();

        locationclient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
        if (ActivityCompat.checkSelfPermission(AppActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(),
                    "Permision not granted",
                    Toast.LENGTH_SHORT)
                    .show();
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 200);

        }else{
//New implementation that checks if the location is available and waits until it retrieves it
            locationclient.getLocationAvailability().addOnSuccessListener(new OnSuccessListener<LocationAvailability>() {
                @Override
                public void onSuccess(LocationAvailability locationAvailability) {
                    if (locationAvailability.isLocationAvailable()) {

                        if (ActivityCompat.checkSelfPermission(AppActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            Toast.makeText(getApplicationContext(),
                                    "Permision not granted",
                                    Toast.LENGTH_SHORT)
                                    .show();
                            ActivityCompat.requestPermissions(AppActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 200);
                        }else{
                            Task<Location> locationTask = locationclient.getLastLocation();
                            locationTask.addOnCompleteListener(new OnCompleteListener<Location>() {
                                @Override
                                public void onComplete(@NonNull Task<Location> task) {
                                    Location location = task.getResult();
                                    setCityNameAndCheck thread2 = new setCityNameAndCheck(location);
                                    thread2.start();
                                    try {
                                        thread2.join();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });


                        }

                    }
                }
            });

        //for the mobility recognition
            if(isPlayServiceAvailable()) {
                buildGoogleApiClient();
            }else{
                Toast.makeText(this, "Google Play Service not Available", Toast.LENGTH_LONG).show();
            }

            //Fragment handling
        dopaminefragment = new dopamineFragment();
        setFragment(dopaminefragment);
        honeyFragment = new HoneyFragment();
        tourismFragment = new TourismFragment();
        eventsFragment = new EventsFragment();
        HiveRequestFragment = new hiveRequestFragment();
        profileFragment = new ProfileFragment();
        newsFragment= new NewsFragment();

        mainNav = findViewById(R.id.main_nav);
        mainNav.setItemIconTintList(null);

        mainNavTop = findViewById(R.id.main_nav_top);
        mainNavTop.setItemIconTintList(null);
        mainNavTop.setVisibility(View.INVISIBLE);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                mainNavTop.setSelectedItemId(R.id.event_nav_button_top);
                mainNav.setSelectedItemId(R.id.dopamine_nav_button);
                mainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.dopamine_nav_button:
                               /// if (city_is_beehive && !nolocation) {
                                    mainNav.setItemBackgroundResource(R.color.colorAccent);
                                    setFragment(dopaminefragment);
                                    if( mainNavTop.getVisibility() == View.VISIBLE){
                                        mainNavTop.setVisibility(View.INVISIBLE);
                                    }
                                return true;

                            case R.id.event_nav_button:
                                if (city_is_beehive && eventactive) {
                                    mainNavTop.setSelectedItemId(R.id.event_nav_button_top);
                                        mainNav.setItemBackgroundResource(R.color.colorAccent);
                                        setFragment(eventsFragment);
                                    if( mainNavTop.getVisibility() == View.INVISIBLE){
                                        mainNavTop.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    mainNavTop.setSelectedItemId(R.id.learn_nav_button);
                                    mainNav.setItemBackgroundResource(R.color.colorAccent);
                                    setFragment(newsFragment);
                                    if( mainNavTop.getVisibility() == View.INVISIBLE){
                                        mainNavTop.setVisibility(View.VISIBLE);
                                    }
                                }
                                return true;
                            case R.id.store_nav_button:
                              //  if (city_is_beehive) {
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean("city_is_hive", city_is_beehive);
                                    bundle.putBoolean("nolocation", nolocation);
                                    bundle.putBoolean("activestore", storeactive);
                                    honeyFragment.setArguments(bundle);
                                    mainNav.setItemBackgroundResource(R.color.colorAccent);
                                    setFragment(honeyFragment);
                                if( mainNavTop.getVisibility() == View.VISIBLE){
                                    mainNavTop.setVisibility(View.INVISIBLE);
                                }
                                return true;
                            case R.id.profile_nav_button:
                                mainNav.setItemBackgroundResource(R.color.colorAccent);
                                setFragment(profileFragment);
                                if( mainNavTop.getVisibility() == View.VISIBLE){
                                    mainNavTop.setVisibility(View.INVISIBLE);
                                }
                                return true;
                            default:
                                return false;
                        }
                    }
                });

                //Top navigation for challenges
                mainNavTop.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.learn_nav_button:
                                setFragment(newsFragment);
                                return true;
                            case R.id.places_nav_button:
                                if (city_is_beehive && placesactive) {
                                    setFragment(tourismFragment);

                                } else {
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean("nolocation", nolocation);
                                    HiveRequestFragment.setArguments(bundle);
                                    setFragment(HiveRequestFragment);
                                }
                                return true;
                            case R.id.event_nav_button_top:
                                if (city_is_beehive && eventactive) {
                                    setFragment(eventsFragment);
                                } else {
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean("nolocation", nolocation);
                                    HiveRequestFragment.setArguments(bundle);
                                    setFragment(HiveRequestFragment);
                                }
                                return true;
                            default:
                                return false;
                        }
                    }
                });


            }
        }, 1000);
    }
    }



//NAME OF THE CITY Check and add to shared preferences
    class setCityNameAndCheck extends Thread {
        Location location;
        setCityNameAndCheck(Location location){
            this.location= location;
        }
    @Override
    public void run() {
      if(location!=null) {


          city_name = getCity(location.getLatitude(), location.getLongitude());
          if(city_name!="no") {

            //check if the city is part of the cities in the list of Urbanbees
              cityRef = FirebaseDatabase.getInstance().getReference();
              cityRef.child("Cities").
                      addListenerForSingleValueEvent(new ValueEventListener() {
                          @Override
                          public void onDataChange(DataSnapshot dataSnapshot) {

                              if(dataSnapshot.child(city_name).exists() && dataSnapshot.child(city_name).child("active").getValue().equals("yes")){
                                  city_is_beehive = true;
                                  if(dataSnapshot.child(city_name).child("Events").child("active").getValue().equals("yes")){
                                      eventactive=true;
                                  }
                                  if(dataSnapshot.child(city_name).child("Products").child("active").getValue().equals("yes")){
                                      storeactive=true;
                                  }
                                  if(dataSnapshot.child(city_name).child("Tplaces").child("active").getValue().equals("yes")){
                                      placesactive=true;
                                  }
                              }

                          }


                          @Override
                          public void onCancelled(DatabaseError databaseError) {
                              Toast.makeText(getApplicationContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                          }
                      });

          }else{
              location=null;
          }

      }
        if(location==null){
            SharedPreferences sharedPreferences2 = getSharedPreferences("nolocation", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences2.edit();
            editor.putString("nolocation", "true");
            nolocation=true;
        }
        SharedPreferences sharedPreferences = getSharedPreferences("city", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("city", city_name);
        editor.apply();
    }
}



    //mobility recognition
    class activitiesMobilityRecognize extends Thread{
        @Override
        public void run(){
            Intent intent=new Intent (AppActivity.this, ActivityRecognizedService.class);
            pendingIntent= PendingIntent.getService(AppActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(mApiClient, 90000,pendingIntent);
        }
    }
    //mobility recognition
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        activitiesMobilityRecognize thread = new activitiesMobilityRecognize();
        thread.start();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(getApplicationContext(),
                "Requesting activity updates suspended",
                Toast.LENGTH_SHORT)
                .show();
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getApplicationContext(),
                "Connection failed",
                Toast.LENGTH_SHORT)
                .show();
    }
    //mobility recognition
    ActivityTransitionRequest buildTransitionRequest() {
        List transitions = new ArrayList<>();
        transitions.add(new ActivityTransition.Builder()
                .setActivityType(DetectedActivity.ON_BICYCLE)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build());
        transitions.add(new ActivityTransition.Builder()
                .setActivityType(DetectedActivity.ON_BICYCLE)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build());
        transitions.add(new ActivityTransition.Builder()
                .setActivityType(DetectedActivity.WALKING)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build());
        transitions.add(new ActivityTransition.Builder()
                .setActivityType(DetectedActivity.WALKING)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build());
        transitions.add(new ActivityTransition.Builder()
                .setActivityType(DetectedActivity.RUNNING)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build());
        transitions.add(new ActivityTransition.Builder()
                .setActivityType(DetectedActivity.RUNNING)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build());
        return new ActivityTransitionRequest(transitions);

    }
    protected synchronized void buildGoogleApiClient() {
        mApiClient = new GoogleApiClient.Builder(AppActivity.this)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(AppActivity.this)
                .addOnConnectionFailedListener(AppActivity.this)
                .build();
        mApiClient.connect();
    }
    private boolean isPlayServiceAvailable() {
        return GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS;
    }
    //lenguage check
    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        if(Build.VERSION.SDK_INT >=Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(locale);
        }else {
            config.locale = locale;
        }
        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor= getSharedPreferences("settings", Context.MODE_PRIVATE).edit();
        editor.putString("language",lang);
        editor.apply();
    }
    public void loadLocale(){
        SharedPreferences prefs= getSharedPreferences("settings", Context.MODE_PRIVATE);
        String language= prefs.getString("language", "de");
        setLocale(language);
    }

//
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 200: {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            }
        }
    }

    //settings on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction= getSupportFragmentManager().beginTransaction();

        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }

    private String getCity(double lat, double lon ){
        String city = "";
        Geocoder geoCoder = new Geocoder(getBaseContext(), Locale.getDefault());
        try {
            List<Address> addresses = geoCoder.getFromLocation(lat, lon, 1);
            if (addresses.size() > 0) {
                for (Address adr:addresses){
                    if(adr.getLocality() !=null && adr.getLocality().length() > 0){
                        city= adr.getLocality();
                        break;
                    }
                }
            }

        }
        catch (IOException e1) {
            e1.printStackTrace();
            city="no";
        }
        return city;
    }



    public void onDestroy() {
        super.onDestroy();
        if(activityRecognitionClient!=null){
           activityRecognitionClient.removeActivityUpdates(pendingIntent);
        }
    }
}
