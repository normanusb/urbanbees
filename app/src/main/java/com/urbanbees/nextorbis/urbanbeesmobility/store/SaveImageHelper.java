package com.urbanbees.nextorbis.urbanbeesmobility.store;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.urbanbees.nextorbis.urbanbeesmobility.R;

import java.lang.ref.WeakReference;

public class SaveImageHelper implements Target {
    private Context context;
    private WeakReference<AlertDialog> alertDialogWeakReference;
    private WeakReference<ContentResolver> contentResolverWeakReference;
    private String name, desc ;
    ImageButton dialog_button;
    TextView dialoginformation, dialogtitle;
    Dialog welcomingDialog1;


    public SaveImageHelper(Context context, AlertDialog alertDialog, ContentResolver contentResolver, String name, String desc) {
        this.context= context;
        this.alertDialogWeakReference = new WeakReference<AlertDialog>(alertDialog);
        this.contentResolverWeakReference = new WeakReference<ContentResolver>(contentResolver);
        this.name = name;
        this.desc = desc;
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        ContentResolver r= contentResolverWeakReference.get();
        AlertDialog dialog= alertDialogWeakReference.get();
        if(r != null){
            MediaStore.Images.Media.insertImage(r,bitmap,name,desc);
        }
        dialog.dismiss();

        //dialog for claiming an already bought product that has to be claimed in a place

        Okdialog("Congratulations!","Your new poster was saved in the storage of your phone",true);

    }

    @Override
    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
        AlertDialog dialog= alertDialogWeakReference.get();

        Toast.makeText(context, "Error"+e.toString(),Toast.LENGTH_LONG).show();

        dialog.dismiss();
    }
    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {

    }
    public void Okdialog(String title, String information, final boolean cancel){
        welcomingDialog1= new Dialog(context);
        welcomingDialog1.setContentView(R.layout.popup_notification);

        dialoginformation = welcomingDialog1.findViewById(R.id.text_description);
        dialogtitle = welcomingDialog1.findViewById(R.id.information_text);
        dialog_button= welcomingDialog1.findViewById(R.id.button_ok);

        dialogtitle.setText(title);
        dialoginformation.setText(information);

        dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                welcomingDialog1.dismiss();
                if(cancel){
                    Activity activity= (Activity)context;
                    activity.finish();
                }
            }
        });
        welcomingDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        welcomingDialog1.show();
    }

}