package com.urbanbees.nextorbis.urbanbeesmobility.store;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.urbanbees.nextorbis.urbanbeesmobility.R;

import java.util.HashMap;
import java.util.Map;
public class HoneyFragment extends Fragment {

    ListView listview_honey;
    ImageView honey_progress_image;
    FirebaseListAdapter fireadapter;
    ProgressBar spinner;
    Map<String, String> TProductsid = new HashMap<String, String>();
    TextView text_honey,text_donations,text_donations_info,dialoginformation, dialogtitle,dialogyesnotitle,dialogyesnoinformation;
    FirebaseAuth auth;
    FirebaseUser user;
    DatabaseReference reference,itemRef;
    String user_id, city_name,urlproduct;
    int costOfProduct;
    Button donate_button;
    Dialog welcomingDialog1,Donatefunctionality1;
    ImageButton dialog_button,dialogyes_button,dialogno_button;
    String urbees;
    int donations;
    Boolean isCityHive,storeactive=false;
    int deviceWidth, deviceHeight;
    LinearLayout imagelinearlayout;
    FrameLayout listview_products_container;
    Activity activity1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_honey, container, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState){
        urbees="0";

        activity1 = getActivity();

        if(isAdded() && activity1 !=null) {

            //Determine width and height of the screen for the images from firebase
            widhandheight thread = new widhandheight();
            thread.start();

            SharedPreferences sharedPreferences = activity1.getSharedPreferences("city", activity1.MODE_PRIVATE);
            city_name = sharedPreferences.getString("city", "");
            spinner = view.findViewById(R.id.progressBarHoney);
            spinner.setVisibility(View.VISIBLE);
            //for the donation area
            text_honey = view.findViewById(R.id.text_honey);
            text_donations = view.findViewById(R.id.text_donations);
            text_donations_info = view.findViewById(R.id.text_donations_info);
            honey_progress_image = view.findViewById(R.id.honey_progress_image);
            listview_products_container=view.findViewById(R.id.listview_products_container);
            listview_honey = view.findViewById(R.id.listview_honey);
            donate_button = view.findViewById(R.id.donate_button);

            TextViewCompat.setAutoSizeTextTypeWithDefaults(text_honey,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(text_donations,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(text_donations_info,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            donate_button.setEnabled(false);
            auth = FirebaseAuth.getInstance();
            user = auth.getCurrentUser();
            user_id = user.getUid();


            //alert dialog for donations
            Donatefunctionality1= new Dialog(activity1);
            Donatefunctionality1.setContentView(R.layout.popup_yesorno);

            dialogyesnoinformation = Donatefunctionality1.findViewById(R.id.text_description_2);
            dialogyesnotitle = Donatefunctionality1.findViewById(R.id.information_text_2);
            dialogyes_button= Donatefunctionality1.findViewById(R.id.buttonyes);
            dialogno_button= Donatefunctionality1.findViewById(R.id.buttonno);

            dialogyesnotitle.setText(getString(R.string.donate_title));
            dialogyesnoinformation.setText(getString(R.string.donate_text));

            dialogyes_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Donating();
                    Donatefunctionality1.dismiss();
                }
            });
            dialogno_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Donatefunctionality1.dismiss();
                }
            });

            reference = FirebaseDatabase.getInstance().getReference();
            //Set the donation bar
            setDonationBar donationBar = new setDonationBar();
            new Thread(donationBar).start();



            //Donation code
            donate_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    Donatefunctionality1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Donatefunctionality1.show();
                }
            });
            //check if the city detected is part of the urbanbees comunity
            isCityHive = getArguments().getBoolean("city_is_hive");
            storeactive = getArguments().getBoolean("activestore");
            if (isCityHive && storeactive) {
                //load all the products of the store
                Query query = reference.child("Cities").child(city_name).child("Products").orderByChild("expired").equalTo("no");

                FirebaseListOptions<Products> options = new FirebaseListOptions.Builder<Products>()
                        .setLayout(R.layout.event_layout2)
                        .setLifecycleOwner(getActivity())
                        .setQuery(query, Products.class)
                        .build();

                fireadapter = new FirebaseListAdapter(options) {
                    @Override
                    protected void populateView(View v, Object model, int position) {
                        imagelinearlayout = v.findViewById(R.id.imagelinearlayout1);
                        final TextView productname = v.findViewById(R.id.textView_descript_event);
                        final TextView productinfo = v.findViewById(R.id.textView_info_event);
                        final TextView event_button_text=v.findViewById(R.id. button_event_check_text);

                        //TextViewCompat.setAutoSizeTextTypeWithDefaults(productname,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
                        //TextViewCompat.setAutoSizeTextTypeWithDefaults(productinfo,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);

                        if(productname.getText().toString().equals("") ){

                            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(productname, 30, 60, 1, TypedValue.COMPLEX_UNIT_PX);

                            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(productinfo, 40, 60, 1, TypedValue.COMPLEX_UNIT_PX);

                            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(event_button_text, 25, 40, 1, TypedValue.COMPLEX_UNIT_PX);
                        }
                        ImageButton productButton = v.findViewById(R.id.button_event_check);
                        ImageButton productButtonFake = v.findViewById(R.id.button_event_check_fake);

                        TextView productButtonText = v.findViewById(R.id.button_event_check_text);
                        ImageView imageproduct = v.findViewById(R.id.imageEventPlace);
                        ViewGroup.LayoutParams params = imagelinearlayout.getLayoutParams();
                        params.width = deviceWidth;
                        imagelinearlayout.setLayoutParams(params);

                        Products products = (Products) model;
                        productname.setText(products.getName().toString());
                        productinfo.setText(products.getDescription());
                        productinfo.setVisibility(View.GONE);
                        costOfProduct = products.getUrbees();
                        urlproduct = products.getLink();
                        if (costOfProduct == -1) {
                            productButtonText.setText(getString(R.string.contact_button));

                        } else if (costOfProduct > 0) {
                            productButtonText.setText((Integer.toString(products.getUrbees())));

                        }
                       // Picasso.get().load(products.getImage().toString()).resize(deviceWidth, deviceHeight/6).placeholder(R.drawable.chargepage).into(imageproduct, new com.squareup.picasso.Callback() {
                         Picasso.get().load(products.getImage().toString()).placeholder(R.drawable.chargepage).into(imageproduct, new com.squareup.picasso.Callback() {

                                @Override
                            public void onSuccess() {
                                productname.setWidth(0);
                                productinfo.setWidth(0);
                                imagelinearlayout.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError(Exception e) {

                            }
                        });
                        //to get the id
                        final String productname_value = productname.getText().toString();
                        itemRef = getRef(position);
                        TProductsid.put(productname_value, itemRef.getKey());
                        //  Product_position_in_array= TProductsid.indexOf(itemRef.getKey());
                        if (costOfProduct == -1) {
                            productButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View arg0) {
                                    Intent browsermaps = new Intent(Intent.ACTION_VIEW, Uri.parse(urlproduct));
                                    startActivity(browsermaps);
                                }
                            });
                            productButtonFake.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View arg0) {
                                    Intent browsermaps = new Intent(Intent.ACTION_VIEW, Uri.parse(urlproduct));
                                    startActivity(browsermaps);
                                }
                            });

                        } else if (costOfProduct > 0) {

                            if (costOfProduct > Integer.parseInt(urbees)) {
                                productButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View arg0) {
                                        Intent goPrdocuctPage = new Intent(activity1, InfoActivity.class);
                                        goPrdocuctPage.putExtra("productID", TProductsid.get(productname_value));
                                        goPrdocuctPage.putExtra("type", "product");
                                        goPrdocuctPage.putExtra("active", "no");
                                        startActivity(goPrdocuctPage);
                                    }
                                });
                                productButtonFake.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View arg0) {
                                        Intent goPrdocuctPage = new Intent(activity1, InfoActivity.class);
                                        goPrdocuctPage.putExtra("productID", TProductsid.get(productname_value));
                                        goPrdocuctPage.putExtra("type", "product");
                                        goPrdocuctPage.putExtra("active", "no");
                                        startActivity(goPrdocuctPage);
                                    }
                                });


                            } else {
                                productButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View arg0) {
                                        Intent goPrdocuctPage = new Intent(activity1, InfoActivity.class);
                                        goPrdocuctPage.putExtra("productID", TProductsid.get(productname_value));
                                        goPrdocuctPage.putExtra("type", "product");
                                        goPrdocuctPage.putExtra("active", "yes");
                                        startActivity(goPrdocuctPage);
                                    }
                                });
                                productButtonFake.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View arg0) {
                                        Intent goPrdocuctPage = new Intent(activity1, InfoActivity.class);
                                        goPrdocuctPage.putExtra("productID", TProductsid.get(productname_value));
                                        goPrdocuctPage.putExtra("type", "product");
                                        goPrdocuctPage.putExtra("active", "yes");
                                        startActivity(goPrdocuctPage);
                                    }
                                });
                            }
                        }
                        //for loading time
                        spinner.setVisibility(View.GONE);

                    }
                };
                listview_honey.setAdapter(fireadapter);
            } else {
                spinner.setVisibility(View.GONE);
                listview_honey.setVisibility(View.GONE);

                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        0,
                        0
                );
                listview_products_container.setLayoutParams(param);

                Okdialog(getString(R.string.no_prodcuts_text),getString(R.string.no_products_in_store));
            }

        }//end of the if for the fragment context
    }

    @Override
    public void onStart() {
        super.onStart();
        if(isCityHive && storeactive) {
            fireadapter.startListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(isCityHive && storeactive) {
            fireadapter.stopListening();
        }
    }

    public void Donating(){
        reference= FirebaseDatabase.getInstance().getReference();
        //for showing the amount of urbees
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 urbees= dataSnapshot.child("Users").child(user_id).child("urbees").getValue().toString();
                 int urbees_amount=Integer.parseInt(urbees);
                 if(urbees_amount>60){
                     int newAmountUrbees= urbees_amount-60;
                     reference.child("Users").child(user_id).child("urbees").setValue(newAmountUrbees);
                     if(dataSnapshot.child("Donations").child("Users").child(user_id).exists()) {
                         int valuedonation = Integer.parseInt(dataSnapshot.child("Donations").child("Users").child(user_id).getValue().toString());
                         int newvalueDonation= valuedonation+60;
                         reference.child("Donations").child("Users").child(user_id).setValue(newvalueDonation);
                     }else{
                         reference.child("Donations").child("Users").child(user_id).setValue(60);
                     }
                     int amountdonated= Integer.parseInt(dataSnapshot.child("Donations").child("Amount").getValue().toString());
                     int newAmountDonated= amountdonated+60;
                     reference.child("Donations").child("Amount").setValue(newAmountDonated);

                     Okdialog(getString(R.string.thanks_donate_title),getString(R.string.thanks_donate_text));
                 }else{
                     Okdialog(getString(R.string.not_enough_donate_title),getString(R.string.not_enough_donate_text));
                 }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                if(isAdded() && activity1 !=null) {
                    Toast.makeText(activity1, databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    class setDonationBar implements Runnable{
        @Override
        public void run(){

            //for showing the amount of urbees
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    String userIsNew = dataSnapshot.child("Users").child(user_id).child("firsttimestore").getValue().toString();
                    if (userIsNew.equals("yes")) {

                        Okdialog(getString(R.string.welcome_title_store),getString(R.string.welcome_text_store));
                        reference.child("Users").child(user_id).child("firsttimestore").setValue("no");
                    }

                    //amount of urbees the user has
                    urbees= dataSnapshot.child("Users").child(user_id).child("urbees").getValue().toString();
                    text_honey.setText(urbees);

                    //to set the bar amount of donations
                    donations= Integer.parseInt(dataSnapshot.child("Donations").child("Amount").getValue().toString());
                    if(donations<25200){
                        honey_progress_image.setImageResource(R.drawable.donationbar1);
                    }else if(donations<50400){
                        honey_progress_image.setImageResource(R.drawable.donationbar2);
                    }else if(donations<75600){
                        honey_progress_image.setImageResource(R.drawable.donationbar3);
                    }else if(donations<100800){
                        honey_progress_image.setImageResource(R.drawable.donationbar4);
                    }else if(donations<126000){
                        honey_progress_image.setImageResource(R.drawable.donationbar5);
                    }else if(donations<151200){
                        honey_progress_image.setImageResource(R.drawable.donationbar6);
                    }else if(donations<176400){
                        honey_progress_image.setImageResource(R.drawable.donationbar7);
                    }else if(donations<201600){
                        honey_progress_image.setImageResource(R.drawable.donationbar8);
                    }else if(donations<226800){
                        honey_progress_image.setImageResource(R.drawable.donationbar9);
                    }else if(donations>=252000){
                        honey_progress_image.setImageResource(R.drawable.donationbar10);
                    }

                    if(donations>=252000){
                        if(isAdded()){
                        text_donations.setText(getString(R.string.donate_bar_text_full1)+"\n"+donations+" "+getString(R.string.donate_bar_text_full2));
                        text_donations_info.setText(getString(R.string.donate_bar_text_full3));
                        }

                    }else{
                        if(isAdded()){
                        text_donations.setText(getString(R.string.donate_bar_text_not_yet_full1)+"\n"+donations+" "+getString(R.string.donate_bar_text_not_yet_full2));
                        text_donations_info.setText(getString(R.string.donate_bar_text_not_yet_full3));
                        }
                    }
                    donate_button.setEnabled(true);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    if(isAdded() && activity1 !=null) {
                        Toast.makeText(activity1, databaseError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });

        }

    }
    //Determine width and height of the screen for the images from firebase
    class widhandheight extends Thread {

        @Override
        public void run() {
            if(isAdded() && activity1 !=null) {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                WindowManager windowmanager = (WindowManager) activity1.getSystemService(Context.WINDOW_SERVICE);
                windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
                deviceWidth = Math.round(displayMetrics.widthPixels );
                deviceHeight = displayMetrics.heightPixels;

            }
        }
    }
    public void Okdialog(String title, String information){
        welcomingDialog1= new Dialog(activity1);
        welcomingDialog1.setContentView(R.layout.popup_notification);

        dialoginformation = welcomingDialog1.findViewById(R.id.text_description);
        dialogtitle = welcomingDialog1.findViewById(R.id.information_text);
        dialog_button= welcomingDialog1.findViewById(R.id.button_ok);

        dialogtitle.setText(title);
        dialoginformation.setText(information);

        dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                welcomingDialog1.dismiss();
            }
        });
        welcomingDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        welcomingDialog1.show();
    }

}


