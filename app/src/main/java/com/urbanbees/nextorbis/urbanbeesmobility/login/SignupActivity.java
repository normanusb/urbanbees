package com.urbanbees.nextorbis.urbanbeesmobility.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.urbanbees.nextorbis.urbanbeesmobility.R;
import com.urbanbees.nextorbis.urbanbeesmobility.main.AppActivity;

public class SignupActivity extends AppCompatActivity {
    ImageButton btsignup;
    EditText email, password1, password2, name;
    FirebaseAuth auth;
    FirebaseUser firebaseuser;
    DatabaseReference rootReference;
    AlertDialog.Builder datenschutz;
    TextView btsignup_text;
    boolean userexist= false;
    boolean datapolicy= false;
    ProgressBar spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
       // getSupportActionBar().hide();

        btsignup= findViewById(R.id.btsignup);
        email=  findViewById(R.id.etEmailsignup);
        password1= findViewById(R.id.etpasssignup1);
        password2= findViewById(R.id.etpasssignup2);
        name= findViewById(R.id.etNamesignup);
        btsignup_text= findViewById(R.id.btsignup_text);
        spinner = findViewById(R.id.progresssigup);
        spinner.setVisibility(View.GONE);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(btsignup_text,25,40,1, TypedValue.COMPLEX_UNIT_PX);

        auth= FirebaseAuth.getInstance();
        rootReference = FirebaseDatabase.getInstance().getReference().child("Users");

        btsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser();

            }
        });

    }

    public void createUser(){

        //Alert dialog for reservations
        datenschutz = new AlertDialog.Builder(SignupActivity.this,R.style.AlertDialogTheme);
        datenschutz.setMessage(getString(R.string.datenschutzenglish)).setCancelable(false)
                .setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        datapolicy=true;
                        dialogInterface.cancel();
                        btsignup.setEnabled(false);
                        auth.createUserWithEmailAndPassword(email.getText().toString(), password1.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(getApplicationContext(), "User created Succesfully", Toast.LENGTH_SHORT).show();
                                    //Save in database
                                    firebaseuser = auth.getCurrentUser();
                                    User myUserInsertObj = new User(email.getText().toString(), name.getText().toString(), "yes", "yes", "yes", "yes","yes", 0, 0, 0, 0);
                                    rootReference.child(firebaseuser.getUid()).setValue(myUserInsertObj)
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        // Make the name the display name of the User in the app
                                                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                                                .setDisplayName(name.getText().toString()).build();

                                                        firebaseuser.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    Toast.makeText(getApplicationContext(), "Name added", Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        });

                                                        Toast.makeText(getApplicationContext(), "User created and saved", Toast.LENGTH_SHORT).show();
                                                        spinner.setVisibility(View.GONE);
                                                        finish();
                                                        gotoApp();
                                                    } else {
                                                        spinner.setVisibility(View.GONE);
                                                        String e = task.getException().toString();
                                                        Log.d("Database failed", e + "");
                                                        Toast.makeText(getApplicationContext(), "User not saved  ", Toast.LENGTH_SHORT).show();
                                                        btsignup.setEnabled(true);
                                                    }
                                                }
                                            });


                                } else {
                                    FirebaseAuthException e = (FirebaseAuthException) task.getException();
                                    Toast.makeText(getApplicationContext(), e.getMessage() + "User could not be created", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });





                    }
                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
                finish();
            }
        });

        String emailforUser= email.getText().toString();
        String password= password1.getText().toString();
        String secondpassword= password2.getText().toString();
        String nameUser = name.getText().toString();
        if(emailforUser.equals("")|| password.equals("") ||secondpassword.equals("")||nameUser.equals(""))
        {
            Toast.makeText(getApplicationContext(),"Blank not allowed",Toast.LENGTH_SHORT).show();
        }
        else if(!Patterns.EMAIL_ADDRESS.matcher(emailforUser).matches()){
            Toast.makeText(getApplicationContext(),"please enter a valid email",Toast.LENGTH_SHORT).show();
        }
        else if( !password.equals(secondpassword)) {
            Toast.makeText(getApplicationContext(), "Passwords are not equal", Toast.LENGTH_SHORT).show();
        }else if(password.length()<6){
            Toast.makeText(getApplicationContext(), "Passwords must be longer than 6 digits", Toast.LENGTH_SHORT).show();
        }
        else{
            auth.fetchSignInMethodsForEmail(emailforUser).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
                @Override
                public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                    spinner.setVisibility(View.VISIBLE);
                    userexist = !(task.getResult().getSignInMethods().isEmpty());
                    if (userexist == true) {
                        Toast.makeText(getApplicationContext(), "user exist already", Toast.LENGTH_SHORT).show();
                        spinner.setVisibility(View.GONE);
                    } else {
                        AlertDialog alert = datenschutz.create();
                        alert.setTitle(getString(R.string.datenschutzenglishTitle));
                        alert.show();
                    }
                }

            });

        }
    }
    public void  gotoApp () {
        //finish all the activies before this one and this one
        finishAffinity();
        Intent intent = new Intent(this, AppActivity.class);
        startActivity(intent);

    }

}
