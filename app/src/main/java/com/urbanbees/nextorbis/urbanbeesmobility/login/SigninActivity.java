package com.urbanbees.nextorbis.urbanbeesmobility.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.urbanbees.nextorbis.urbanbeesmobility.R;
import com.urbanbees.nextorbis.urbanbeesmobility.main.AppActivity;

public class SigninActivity extends AppCompatActivity {

    ImageButton btlogin;
    TextView btLogin_text;
    EditText email, password;
    FirebaseAuth auth;
    ProgressBar spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        //getSupportActionBar().hide();
        btLogin_text= findViewById(R.id.btLogin_text);
        btlogin=  findViewById(R.id.btLogin);
        email=  findViewById(R.id.etEmaillogin);
        password= findViewById(R.id.etpasslogin);
        spinner = findViewById(R.id.progresslogin);
        spinner.setVisibility(View.GONE);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(btLogin_text,25,40,1, TypedValue.COMPLEX_UNIT_PX);

        auth=FirebaseAuth.getInstance();

        btlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();

            }
        });

    }
   public void loginUser() {

       if (email.getText().toString().equals("") || password.getText().toString().equals("")) {
           Toast.makeText(getApplicationContext(), "Blank not allowed", Toast.LENGTH_SHORT).show();

       } else {
           spinner.setVisibility(View.VISIBLE);
           btlogin.setEnabled(false);
           auth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
               @Override
               public void onComplete(@NonNull Task<AuthResult> task) {
                   if (task.isSuccessful()) {
                       spinner.setVisibility(View.GONE);
                       finish();
                       gotoApp();
                   } else {
                       spinner.setVisibility(View.GONE);
                       Toast.makeText(getApplicationContext(), "Incorrect User", Toast.LENGTH_SHORT).show();
                       btlogin.setEnabled(true);
                   }

               }
           });
       }

   }
    public void gotoApp () {
        //finish all the activies before this one and this one
        finishAffinity();
        Intent intent = new Intent(this, AppActivity.class);
        startActivity(intent);

    }

}