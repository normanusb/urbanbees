package com.urbanbees.nextorbis.urbanbeesmobility.challenges;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.urbanbees.nextorbis.urbanbeesmobility.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Info_events_Activity extends AppCompatActivity {
    //for test log
    private static final String TAG = "UserLocation";
    //location variables
    private FusedLocationProviderClient locationclient;
    Location locationPlace = new Location("Place");
    //database variables
    FirebaseAuth auth;
    FirebaseUser user;
    DatabaseReference reference;
    DataSnapshot mainSnapshop;
    //items in the view
    EditText event_code_et;
    ImageButton button_go, button_more_info,button_reserve_event, location_event,dialog_button,dialogyes_button,dialogno_button;;
    ImageView imageplace;
    TextView place_name_txt, place_info_txt, place_info2_txt,reserve_event_text,go_info_event_text, location_event_text,moreinfo_event_text,dialoginformation, dialogtitle,dialogyesnotitle,dialogyesnoinformation;
    int deviceWidth, deviceHeight;
    //variables for business logic of both types
    Double  place_lat,place_lon;
    String type, place_link,place_urbees,
            user_went, placeId,
            place_name,place_address,
            place_description,place_image,
            user_id,
    //variables for business logic of events;
            evnt_code,place_users_allowed,
            place_start_time,place_finish_time,
            place_date, event_reservation_from,
    //city information
            city_name;
    boolean AlreadyClaimed;
    Dialog welcomingDialog1,Donatefunctionality1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_info_events_);

        //Determine width and height of the screen for the images from firebase
        widhandheight thread = new widhandheight();
        thread.start();

        SharedPreferences sharedPreferences = getSharedPreferences("city", MODE_PRIVATE);
        city_name= sharedPreferences.getString("city","");
        //For the location check
        requestPermission();
        //to check if user already claimed points for this place
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        user_id=user.getUid();
        //setting items in the view
        imageplace = findViewById(R.id.event_image_info);
        button_more_info = findViewById(R.id.moreinfo_event);
        place_name_txt = findViewById(R.id.txtName_events_info);
        place_info_txt = findViewById(R.id.txtInfo_event_info);
        place_info2_txt = findViewById(R.id.txt_event_info2);
        event_code_et= findViewById(R.id.et_event_code);
        event_code_et.setTypeface(Typeface.DEFAULT);
        button_go = findViewById(R.id.go_info_event);
        go_info_event_text=findViewById(R.id.go_info_event_text);
        button_reserve_event=findViewById(R.id.reserve_event);
        reserve_event_text=findViewById(R.id.reserve_event_text);
        location_event=findViewById(R.id.location_event);
        location_event_text=findViewById(R.id.location_event_text);
        moreinfo_event_text=findViewById(R.id.moreinfo_event_text);

        TextViewCompat.setAutoSizeTextTypeWithDefaults(place_name_txt,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        TextViewCompat.setAutoSizeTextTypeWithDefaults(place_info_txt,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        TextViewCompat.setAutoSizeTextTypeWithDefaults(place_info2_txt,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(go_info_event_text,25,50,1, TypedValue.COMPLEX_UNIT_PX);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(location_event_text,25,40,1, TypedValue.COMPLEX_UNIT_PX);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(moreinfo_event_text,25,40,1, TypedValue.COMPLEX_UNIT_PX);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(reserve_event_text,25,40,1, TypedValue.COMPLEX_UNIT_PX);

        //getting what type of view it will be
        type = getIntent().getStringExtra("type");


        if (type.equals("event")) {
            reference = FirebaseDatabase.getInstance().getReference().child("Cities").child(city_name).child("Events");
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //variables on events and turism places
                    placeId  = getIntent().getStringExtra("eventid");
                    mainSnapshop= dataSnapshot.child(placeId);
                    place_name = mainSnapshop.child("name").getValue().toString();
                    place_address = mainSnapshop.child("address ").getValue().toString();
                    place_description = mainSnapshop.child("description").getValue().toString();
                    place_link = mainSnapshop.child("link").getValue().toString();
                    place_urbees = mainSnapshop.child("urbees").getValue().toString();
                    place_image = mainSnapshop.child("image").getValue().toString();
                    //variables only present in events
                    event_reservation_from= mainSnapshop.child("reservation_from").getValue().toString();
                    evnt_code= mainSnapshop.child("code").getValue().toString();
                    place_date = mainSnapshop.child("date").getValue().toString();
                    place_users_allowed = mainSnapshop.child("numberofusers").getValue().toString();
                    place_start_time = mainSnapshop.child("beginen").getValue().toString();
                    place_finish_time = mainSnapshop.child("end").getValue().toString();
                    place_lat=Double.parseDouble(mainSnapshop.child("lat").getValue().toString());
                    place_lon=Double.parseDouble(mainSnapshop.child("lon").getValue().toString());
                    //set location of the place
                    locationPlace.setLatitude(place_lat);
                    locationPlace.setLongitude(place_lon);

                    //Button to check the location of the event in google maps
                    location_event.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intentmaps= new Intent(Intent.ACTION_VIEW);
                            intentmaps.setData(Uri.parse("geo:"+place_lat+","+place_lon+"?q="+place_lat+","+place_lon));
                            Intent chooser= Intent.createChooser(intentmaps," Launch Maps");
                            startActivity(chooser);

                        }
                    });
                    //to check if the user already claimed the urbees or is reserved
                    boolean user_exist=  mainSnapshop.child("Users").child(user_id).exists();
                    if(user_exist){
                        user_went = mainSnapshop.child("Users").child(user_id).child("went").getValue().toString();
                        button_reserve_event.setImageResource(R.drawable.logout);
                        reserve_event_text.setText(getString(R.string.Reserved_event_calcel_button));
                    }else {

                        //CHECK IF is the date to reserve the event
                        if(event_reservation_from.equals("no")){
                            button_reserve_event.setVisibility(View.INVISIBLE);
                        }else{
                            boolean after_date=false;
                            try{
                                after_date=  new SimpleDateFormat("dd.MM.yyyy").parse(event_reservation_from).before(new Date());
                            }catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (after_date) {
                                //ya paso la fecha
                                button_reserve_event.setImageResource(R.drawable.login2);
                                button_reserve_event.setVisibility(View.VISIBLE);
                                reserve_event_text.setText(getString(R.string.reserve));
                                button_reserve_event.setEnabled(true);
                                button_go.setEnabled(true);
                            }else{
                                //no ha pasado la fecha
                                button_reserve_event.setImageResource(R.drawable.login2);
                                button_reserve_event.setVisibility(View.INVISIBLE);
                                go_info_event_text.setVisibility(View.GONE);
                                button_go.setVisibility(View.GONE);
                                reserve_event_text. setText(getString(R.string.Reserved_event_time).toString()+" "+event_reservation_from );
                                button_reserve_event.setEnabled(false);
                                button_go.setEnabled(false);

                            }
                        }

                        user_went="no";
                    }

                    int size_users = 0;
                    for (DataSnapshot snapshot : mainSnapshop.child("Users").getChildren()) {
                        size_users++;
                    }
                    int free_places = Integer.parseInt(place_users_allowed) - size_users;
                    if(free_places==0&&!(reserve_event_text.getText().toString().equals("cancel")))
                    {
                        button_go.setEnabled(false);
                        if(reserve_event_text.getText().toString().equals("reserve")){
                            button_reserve_event.setEnabled(false);
                        }
                    }

                    place_name_txt.setText(place_name);
                    place_info_txt.setText(place_description + " \n "+getString(R.string.date_info)+" " + place_date + " \n "+getString(R.string.schedule_info)+" "+ place_start_time + "-" + place_finish_time + " \n"+getString(R.string.address_info)+" "+ place_address);

                    place_info2_txt.setText(getString(R.string.get_urbees_part1)+" " + place_urbees +""+ getString(R.string.get_urbees_part2)+" \n" +getString( R.string.free_spots_to_reserv1)+" " + free_places + " "+getString(R.string.free_spots_to_reserv2));

                    Picasso.get().load(place_image).resize(deviceWidth*3, deviceHeight/2).into(imageplace);

                    button_more_info.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent browsermaps = new Intent(Intent.ACTION_VIEW, Uri.parse(place_link));
                            startActivity(browsermaps);
                        }
                    });

                    //alert dialog for reservation
                    Donatefunctionality1= new Dialog(Info_events_Activity.this);
                    Donatefunctionality1.setContentView(R.layout.popup_yesorno);
                    dialogyesnoinformation = Donatefunctionality1.findViewById(R.id.text_description_2);
                    dialogyesnotitle = Donatefunctionality1.findViewById(R.id.information_text_2);
                    dialogyes_button= Donatefunctionality1.findViewById(R.id.buttonyes);
                    dialogno_button= Donatefunctionality1.findViewById(R.id.buttonno);

                    dialogyesnotitle.setText(getString(R.string.Reservation_text));
                    dialogyesnoinformation.setText(getString(R.string.are_you_sure_alter_reservation));

                    dialogyes_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            reserve_button_cliecked();
                            Donatefunctionality1.dismiss();
                        }
                    });
                    dialogno_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Donatefunctionality1.dismiss();
                        }
                    });

                    // CHECK IF THE USER ALREADY WAS IN THE PLACE
                    if(user_went!=null &&user_went.equals("yes")){
                        AlreadyClaimed=true;
                    }else {
                        AlreadyClaimed = false;
                    }
                    if(AlreadyClaimed){
                        go_info_event_text.setText(getString(R.string.already_claimed_button_event));
                        button_go.setEnabled(false);
                        button_reserve_event.setEnabled(false);
                        event_code_et.setVisibility(View.INVISIBLE);

                    }else{
                        //button "the user is there and claims points" functionality
                        button_go.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //check location to see if the user is in the right place yo get Urbees
                               if(evnt_code.equals(event_code_et.getText().toString())){
                                   locate_event();
                               }else{
                                   Okdialog(getString(R.string.wrong_code_title),getString(R.string.wrong_code_text),true);
                               }
                            }
                        });
                      //button for reservation or cancelation
                        button_reserve_event.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Donatefunctionality1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                Donatefunctionality1.show();
                            }
                        });

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
















        } else if (type.equals("tplace")) {
            // turism functionality
            event_code_et.setVisibility(View.INVISIBLE);
            location_event.setVisibility(View.INVISIBLE);
            location_event_text.setVisibility(View.INVISIBLE);
            reserve_event_text.setText(R.string.viewLocation);

            reference = FirebaseDatabase.getInstance().getReference().child("Cities").child(city_name).child("Tplaces");
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    placeId = getIntent().getStringExtra("Tplaceid");
                    mainSnapshop= dataSnapshot.child(placeId);
                    place_name = mainSnapshop.child("name").getValue().toString();
                    place_address = mainSnapshop.child("address").getValue().toString();
                    place_description = mainSnapshop.child("description").getValue().toString();
                    place_link = mainSnapshop.child("link").getValue().toString();
                    place_urbees = mainSnapshop.child("urbees").getValue().toString();
                    place_image = mainSnapshop.child("image").getValue().toString();
                    place_lat=Double.parseDouble(mainSnapshop.child("lat").getValue().toString());
                    place_lon=Double.parseDouble(mainSnapshop.child("lon").getValue().toString());
                    //set location of the place
                    locationPlace.setLatitude(place_lat);
                    locationPlace.setLongitude(place_lon);

                    //to check if the user already claimed the urbees
                    boolean user_exist=  mainSnapshop.child("Users").child(user_id).exists();
                    if(user_exist){
                        user_went = mainSnapshop.child("Users").child(user_id).child("went").getValue().toString();
                    }else {
                        user_went="no";
                    }

                    place_name_txt.setText(place_name);
                    place_info_txt.setText(place_description + " \n"+getString(R.string.address_info)+" " + place_address);
                    //Maybe usefulll for later
                   /* int size_users = 0;
                    for (DataSnapshot snapshot : dataSnapshot.child(placeId).child("Users").getChildren()) {
                        size_users++ ;
                    }*/
                    place_info2_txt.setText(getString(R.string.get_urbees_part1)+" " + place_urbees + " "+getString(R.string.get_urbees_part2));

                    Picasso.get().load(place_image).resize(deviceWidth*3, deviceHeight/2).into(imageplace);

                    button_more_info.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent browsermaps = new Intent(Intent.ACTION_VIEW, Uri.parse(place_link));
                            startActivity(browsermaps);
                        }
                    });

                    //Button to check the location of the event in google maps
                    button_reserve_event.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intentmaps= new Intent(Intent.ACTION_VIEW);
                            intentmaps.setData(Uri.parse("geo:"+place_lat+","+place_lon+"?q="+place_lat+","+place_lon));
                            Intent chooser= Intent.createChooser(intentmaps," Launch Maps");
                            startActivity(chooser);

                        }
                    });


                    //CHECK IF THE USER ALREADY WAS IN THE PLACE
                    if(user_went!=null && user_went.equals("yes")){
                        AlreadyClaimed=true;
                    }else {
                        AlreadyClaimed = false;
                    }
                    if(AlreadyClaimed){
                        go_info_event_text.setText(getString(R.string.already_claimed_button_event));
                        button_go.setEnabled(false);
                    }else {

                        button_go.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //check location to see if the user is in the right place yo get Urbees
                                locate_tplace();
                            }
                        });
                    }


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }
    private  void requestPermission(){
        ActivityCompat. requestPermissions(Info_events_Activity.this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},1);
    }


    public void  locate_event () {
        locationclient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
        if (ActivityCompat.checkSelfPermission(Info_events_Activity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        locationclient.getLastLocation().addOnSuccessListener(Info_events_Activity.this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if (location != null) {
                    if(location.distanceTo(locationPlace)<150){
                        button_go.setEnabled(false);
                        reference = FirebaseDatabase.getInstance().getReference();
                        reference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                int urbees_to_claim= Integer.parseInt(place_urbees);
                                int OldUrbees = Integer.parseInt(dataSnapshot.child("Users").child(user_id).child("urbees").getValue().toString());
                                int OldEventsTimes = Integer.parseInt(dataSnapshot.child("Users").child(user_id).child("eventtimes").getValue().toString());
                                int newEventTimes = OldEventsTimes + 1;
                                int newUrbees = OldUrbees + urbees_to_claim;
                                reference.child("Users").child(user_id).child("urbees").setValue(newUrbees);
                                reference.child("Users").child(user_id).child("eventtimes").setValue(newEventTimes);

                                //to assure now the user claimed the urbees
                                reference.child("Cities").child(city_name).child("Events").child(placeId).child("Users").child(user_id).child("went").setValue("yes");
                                Okdialog(getString(R.string.congratulations_urbees_erned_event_title1)+" "+place_urbees+" "+getString(R.string.congratulations_urbees_erned_event_title2),getString(R.string.congratulations_urbees_erned_event_text),true);
                                go_info_event_text.setText(getString(R.string.already_claimed_button_event));

                                event_code_et.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getApplicationContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                                button_go.setEnabled(true);
                            }
                        });

                    }else{
                        Okdialog(getString(R.string.not_at_distance_title1)+" "+ Math.round(location.distanceTo(locationPlace))+" "+getString(R.string.not_at_distance_title2),getString(R.string.not_at_distance_text),false);
                    }
                } else {

                    Log.d(TAG, "There was an error with the location ");

                }
            }
        });
    }

    public void locate_tplace (){
        //add dialog about the check of the user being there
        locationclient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
        if (ActivityCompat.checkSelfPermission(Info_events_Activity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        locationclient.getLastLocation().addOnSuccessListener(Info_events_Activity.this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if (location != null) {
                    if(location.distanceTo(locationPlace)<150){
                        button_go.setEnabled(false);
                        reference = FirebaseDatabase.getInstance().getReference();
                        reference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                int urbees_to_claim= Integer.parseInt(place_urbees);
                                int OldUrbees = Integer.parseInt(dataSnapshot.child("Users").child(user_id).child("urbees").getValue().toString());
                                int OldTuristictTimes = Integer.parseInt(dataSnapshot.child("Users").child(user_id).child("turisticttimes").getValue().toString());
                                int newTuristictTimes = OldTuristictTimes + 1;
                                int newUrbees = OldUrbees + urbees_to_claim;
                                reference.child("Users").child(user_id).child("urbees").setValue(newUrbees);
                                reference.child("Users").child(user_id).child("turisticttimes").setValue(newTuristictTimes);

                                //to assure now the user claimed the urbees
                                reference.child("Cities").child(city_name).child("Tplaces").child(placeId).child("Users").child(user_id).child("went").setValue("yes");
                                Okdialog(getString(R.string.congratulations_urbees_erned_event_title1)+" "+place_urbees+" "+getString(R.string.congratulations_urbees_erned_event_title2),getString(R.string.congratulations_urbees_erned_event_text),true);
                                go_info_event_text.setText(getString(R.string.already_claimed_button_event));

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getApplicationContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                                button_go.setEnabled(true);
                            }
                        });

                    }else{
                        Okdialog(getString(R.string.not_at_distance_title1)+" "+ Math.round(location.distanceTo(locationPlace))+" "+getString(R.string.not_at_distance_title2),getString(R.string.not_at_distance_text),false);
                    }
                } else {

                    Log.d(TAG, "There was an error with the location ");

                }
            }
        });

    }
    public void reserve_button_cliecked(){

        String text_reservation=reserve_event_text.getText().toString();
        if(text_reservation.equals("reserve")){

            reference = FirebaseDatabase.getInstance().getReference();
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //to assure now the user claimed the urbees
                    reference.child("Cities").child(city_name).child("Events").child(placeId).child("Users").child(user_id).child("went").setValue("no");
                   //dialog for reservation
                    Okdialog(getString(R.string.Reserved_event_title),getString(R.string.Reserved_event_text),false);

                    button_reserve_event.setImageResource(R.drawable.logout);
                    reserve_event_text.setText(getString(R.string.Reserved_event_calcel_button));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });

        }else if(text_reservation.equals(getString(R.string.Reserved_event_calcel_button))){

            reference = FirebaseDatabase.getInstance().getReference();
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //to assure now the user claimed the urbees
                    reference.child("Cities").child(city_name).child("Events").child(placeId).child("Users").child(user_id).removeValue();
                    Okdialog(getString(R.string.Canceled_event_title),getString(R.string.Canceled_event_text),false);
                    button_reserve_event.setImageResource(R.drawable.login2);
                    reserve_event_text.setText(getString(R.string.reserve));
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });

        }

    }
    //Determine width and height of the screen for the images from firebase
    class widhandheight extends Thread {

        @Override
        public void run() {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager windowmanager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
            deviceWidth = Math.round(displayMetrics.widthPixels/3);
            deviceHeight= displayMetrics.heightPixels;

        }
    }
    public void Okdialog(String title, String information, final boolean cancel){
        welcomingDialog1= new Dialog(Info_events_Activity.this);
        welcomingDialog1.setContentView(R.layout.popup_notification);

        dialoginformation = welcomingDialog1.findViewById(R.id.text_description);
        dialogtitle = welcomingDialog1.findViewById(R.id.information_text);
        dialog_button= welcomingDialog1.findViewById(R.id.button_ok);

        dialogtitle.setText(title);
        dialoginformation.setText(information);

        dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                welcomingDialog1.dismiss();
                if(cancel){
                    finish();
                }
            }
        });
        welcomingDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        welcomingDialog1.show();
    }

}
