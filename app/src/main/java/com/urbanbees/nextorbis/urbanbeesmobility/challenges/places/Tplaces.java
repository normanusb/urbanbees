package com.urbanbees.nextorbis.urbanbeesmobility.challenges.places;

public class Tplaces {

    public String address,link,name,description,active,image;
    public double lat,lon;
    public int urbees;


    public Tplaces(){
    }

    public Tplaces(String address,double lat, double lon, String link, String name, String description, String active, String image, int urbees) {

        this.address = address;
        this.link = link;
        this.name = name;
        this.active = active;
        this.description = description;
        this.image = image;
        this.urbees = urbees;
        this.lon= lon;
        this.lat=lat;
    }


    public String getAddress() {
        return address;
    }
    public double getLat() {
        return lat;
    }
    public double getLon() {
        return lon;
    }

    public String getActive() {
        return active;
    }

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public int getUrbees() {
        return urbees;
    }


    public void setAddress(String address) {
        this.address = address;
    }
    public void setLat(double lat) {
        this.lat = lat;
    }
    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setUrbees(int urbees) {
        this.urbees = urbees;
    }

}
