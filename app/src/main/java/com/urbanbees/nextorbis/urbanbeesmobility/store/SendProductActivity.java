package com.urbanbees.nextorbis.urbanbeesmobility.store;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.urbanbees.nextorbis.urbanbeesmobility.R;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendProductActivity extends AppCompatActivity {

    String ProductId, city_name,user_id, userName, sendName, StreetName, PostalcodeName,ExtraName,
    //strings for email sending
            rec,subject,textMessage;
    FirebaseAuth auth;
    FirebaseUser user;
    DatabaseReference reference;
    TextView citytxt, userxt ;
    EditText etName, etStreet, etPostalcode,etExtra;
    ImageButton send;
    AlertDialog.Builder surefunctionality;
    AlertDialog.Builder ConfirmInfoProduct;
    ProgressBar spinner;
    Session session=null;
    ProgressDialog dialogEmailProgress=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_product);

        citytxt= findViewById(R.id.txt_city);
        userxt= findViewById(R.id.txt_username);
        send=  findViewById(R.id.btsendInfo);
        etName=findViewById(R.id.etName);
        etStreet= findViewById(R.id.etStreet);
        etPostalcode= findViewById(R.id.etPostalcode);
        etExtra= findViewById(R.id.etExtra);
        spinner = findViewById(R.id.progressBarpay);
        spinner.setVisibility(View.GONE);
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        user_id=user.getUid();
        userName= user.getDisplayName();
        rec="contacturbanbees@gmail.com";
        subject="Someone purchased";


        ProductId= getIntent().getStringExtra("productID");
        SharedPreferences sharedPreferences = getSharedPreferences("city", MODE_PRIVATE);
        city_name= sharedPreferences.getString("city","");
         //confirmation
        ConfirmInfoProduct = new AlertDialog.Builder(SendProductActivity.this,R.style.AlertDialogTheme);
        ConfirmInfoProduct.setMessage(getString(R.string.info_sended_product_text)).setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        finish();
                    }
                });

        //Alert dialog for reservations
        surefunctionality = new AlertDialog.Builder(SendProductActivity.this,R.style.AlertDialogTheme);
        surefunctionality.setMessage(getString(R.string.are_you_sure_send_product_text)).setCancelable(false)
                .setPositiveButton(getString(R.string.confirm_address), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        spinner.setVisibility(View.VISIBLE);
                        //Functionality
                        reference= FirebaseDatabase.getInstance().getReference();
                        reference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                DataSnapshot dataSnapPodcut= dataSnapshot.child("Cities").child(city_name).child("Products").child(ProductId);
                                DataSnapshot dataSnapUser= dataSnapshot.child("Users").child(user_id);

                                if(Integer.parseInt(dataSnapPodcut.child("amount").getValue().toString())>0) {
                                    int priceProduct = Integer.parseInt(dataSnapPodcut.child("urbees").getValue().toString());
                                    int urbeesUser = Integer.parseInt(dataSnapUser.child("urbees").getValue().toString());
                                    int newUrbeesUser = urbeesUser - priceProduct;
                                    reference.child("Users").child(user_id).child("urbees").setValue(newUrbeesUser);
                                    reference.child("Cities").child(city_name).child("Products").child(ProductId).child("users").child(user_id).child("bought").setValue("yes");
                                    int amountProduct= Integer.parseInt(dataSnapPodcut.child("amount").getValue().toString());
                                    int newamountProduct= amountProduct-1;
                                    reference.child("Cities").child(city_name).child("Products").child(ProductId).child("amount").setValue(newamountProduct);
                                        if(dataSnapPodcut.child("users").child(user_id).child("times").exists()){
                                            int timesUserBougthItem= Integer.parseInt(dataSnapPodcut.child("users").child(user_id).child("times").getValue().toString());
                                            int newTimesUserBougthItem= timesUserBougthItem+1;
                                            reference.child("Cities").child(city_name).child("Products").child(ProductId).child("users").child(user_id).child("times").setValue(newTimesUserBougthItem);
                                        }else{
                                            reference.child("Cities").child(city_name).child("Products").child(ProductId).child("users").child(user_id).child("times").setValue(1);

                                        }
                                        if(newamountProduct==0){
                                            reference.child("Cities").child(city_name).child("Products").child(ProductId).child("expired").setValue("yes");
                                        }

                                    spinner.setVisibility(View.GONE);
                                    textMessage= "Name of reciever: "+sendName+"\n \n Street: "+StreetName+"\n \n Postal code: "+PostalcodeName+"\n \n Extra Information: "+ExtraName ;

                                        //send email
                                    Properties emailProperties= new Properties();
                                    emailProperties.put("mail.smtp.host","smtp.gmail.com");
                                    emailProperties.put("mail.smtp.socketFactory.port","465");
                                    emailProperties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
                                    emailProperties.put("mail.smtp.auth","true");
                                    emailProperties.put("mail.smtp.port","465");

                                    session= Session.getDefaultInstance(emailProperties, new Authenticator() {
                                        @Override
                                        protected PasswordAuthentication getPasswordAuthentication() {
                                            return new PasswordAuthentication("contacturbanbees@gmail.com","DoceDeEspadas12.");
                                        }
                                    });
                                    dialogEmailProgress= ProgressDialog.show(SendProductActivity.this,"", ""+getString(R.string.info_sending_product_title),true);
                                    RetreiveFeedTask task =new RetreiveFeedTask();
                                    task.execute();
                                }else{
                                    reference.child("Cities").child(city_name).child("Products").child(ProductId).child("expired").setValue("yes");
                                    Toast.makeText(getApplicationContext(),getString(R.string.product_not_available),Toast.LENGTH_LONG).show();
                                    finish();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        });



                        dialogInterface.cancel();
                    }
                }).setNegativeButton(getString(R.string.Reserved_event_calcel_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });



        userxt.setText("User: "+userName);
        citytxt.setText("City: "+city_name);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendName= etName.getText().toString();
                StreetName=etStreet.getText().toString();
                PostalcodeName=etPostalcode.getText().toString();
                ExtraName= etExtra.getText().toString();

                if(sendName.length()<2|| sendName.equals(" ")|| StreetName.equals("")||PostalcodeName.equals("")||ExtraName.equals("")) {
                    Toast.makeText(getApplicationContext(), "Please fill up all the information", Toast.LENGTH_LONG).show();
                }else{
                    AlertDialog alert = surefunctionality.create();
                    alert.setTitle("Send information");
                    alert.show();
                }
            }
        });

    }
    class RetreiveFeedTask extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... params){

            try{

                Message message= new MimeMessage(session);
                message.setFrom(new InternetAddress("contacturbanbees@gmail.com"));
                message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(rec));
                message.setSubject(subject);
                message.setContent(textMessage, "text/html; charset=utf-8");
                Transport.send(message);


            }catch (MessagingException e){
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            dialogEmailProgress.dismiss();
            //for loading time
            AlertDialog alert = ConfirmInfoProduct.create();
            alert.setTitle("Information send");
            alert.show();
        }
    }
}
