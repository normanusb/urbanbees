package com.urbanbees.nextorbis.urbanbeesmobility.dopamine;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.urbanbees.nextorbis.urbanbeesmobility.R;


public class dopamineFragment extends Fragment {
    FirebaseAuth auth;
    FirebaseUser user;
    DatabaseReference reference;
    int urbees_bike, urbees_foot, urbees_total;
    ImageButton claim_Urbees,dialog_button, claim_Urbees_taptext;
    ImageView arrow_image3;
    TextView user_urbees_dopaminepage, total_text, City_name_image_dopamine,txt_totalMove_urbees_intro,urbeesamounttext, dialoginformation, dialogtitle;
    String city_name,nolocation,user_id,user_email;
    Thread thread3;
    Dialog welcomingDialog1;
    Activity activity1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dopamine, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {

        activity1 = getActivity();
        if(isAdded() && activity1 !=null) {

            auth = FirebaseAuth.getInstance();
            user = auth.getCurrentUser();
            user_id=user.getUid();

            SharedPreferences sharedPreferences3 = activity1.getSharedPreferences("nolocation", activity1.MODE_PRIVATE);
            nolocation = sharedPreferences3.getString("nolocation", "false");
          if(nolocation.equals("true")) {
              Okdialog(getString(R.string.warining),getString(R.string.locationerror));
          }
            urbeesamounttext=view.findViewById(R.id.urbeesamounttext);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(urbeesamounttext,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            claim_Urbees = view.findViewById(R.id.btClaimUrbeesMove);
            claim_Urbees_taptext = view.findViewById(R.id.btClaimUrbeesMove_taptext);
            arrow_image3= view.findViewById(R.id.arrow_image3);
            SharedPreferences sharedPreferences2 = activity1.getSharedPreferences("city", activity1.MODE_PRIVATE);
            city_name = sharedPreferences2.getString("city", "");
            City_name_image_dopamine = view.findViewById(R.id.City_name_image_dopamine);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(City_name_image_dopamine,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            arrow_image3.setVisibility(View.GONE);
            claim_Urbees.setVisibility(View.GONE);
            claim_Urbees_taptext.setVisibility(View.GONE);
            if (!city_name.equals("no")) {
                City_name_image_dopamine.setText(city_name);
            }else{
                urbeesamounttext.setText(R.string.noconnection);
            }


            user_urbees_dopaminepage= view.findViewById(R.id.user_urbees_dopaminepage);
            txt_totalMove_urbees_intro=view.findViewById(R.id.txt_totalMove_urbees_intro);

            TextViewCompat.setAutoSizeTextTypeWithDefaults(user_urbees_dopaminepage,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(txt_totalMove_urbees_intro,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);


            auth = FirebaseAuth.getInstance();
            user = auth.getCurrentUser();

            total_text = view.findViewById(R.id.txt_totalMove_urbees);
            total_text.setVisibility(View.GONE);

            updateEarnings();
            thread3 = new Thread() {
                @Override
                public void run() {
                    while(!isInterrupted()&& dopamineFragment.this.isVisible()){
                        try {
                            if (activity1 != null) {
                                activity1.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        updateEarnings();
                                    }
                                });
                                Thread.sleep(320000);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

            };
            if (thread3.getState() == Thread.State.NEW) {
                thread3.start();
            }

            reference = FirebaseDatabase.getInstance().getReference().child("Users").child(user.getUid());
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String userIsNew = dataSnapshot.child("firsttime").getValue().toString();

                    String urbees = dataSnapshot.child("urbees").getValue().toString();
                    String nameUser= dataSnapshot.child("name").getValue().toString();
                    user_email= dataSnapshot.child("useremail").getValue().toString();
                    user_urbees_dopaminepage.setText(urbees);

                    //IF AFTER NOT having a connection and getting "no" as a city, suddently the connection comes back, show again the claim button and the coin
                    arrow_image3.setVisibility(View.VISIBLE);
                    claim_Urbees.setVisibility(View.VISIBLE);
                    claim_Urbees_taptext.setVisibility(View.VISIBLE);
                    urbeesamounttext.setText(R.string.taptocolecttext);
                    txt_totalMove_urbees_intro.setText(R.string.totalurbeestext);

                    //username_txt.setText(nameUser);
                    if (userIsNew.equals("yes")) {

                        Okdialog(getString(R.string.welcome_title),getString(R.string.welcome_text));

                        urbees_total += 30;
                        reference.child("firsttime").setValue("no");
                        checkviral();
                    }
                    total_text.setText(""+urbees_total);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(activity1, databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
            claim_Urbees.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                      click_claimurbees_function();
                }
            });
            claim_Urbees_taptext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click_claimurbees_function();
                }
            });


     }
    }

    private void updateEarnings() {
        SharedPreferences sharedPreferences = activity1.getSharedPreferences("Pointshandling", activity1.MODE_PRIVATE);
        urbees_bike = sharedPreferences.getInt("urbees_bike", 0);
        urbees_foot = sharedPreferences.getInt("urbees_foot", 0);
        urbees_total = urbees_bike + urbees_foot;
        total_text.setText(""+urbees_total);
        if(urbees_total==0){
            claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage0);
        }
        else if(urbees_total<10){
            claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage1);
        }else if(urbees_total<50){
            claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage2);
        }else if(urbees_total<100){
            claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage3);
        }else if(urbees_total<200){
            claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage4);
        }else if(urbees_total<300){
            claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage5);
        }else if(urbees_total<400){
            claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage6);
        }else if(urbees_total<700){
            claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage7);
        }else if(urbees_total<1000){
            claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage8);
        }else if(urbees_total<2000){
            claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage9);
        }else if(urbees_total<3500){
            claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage10);
        }

    }
    public void checkviral(){
        Query query = FirebaseDatabase.getInstance().getReference().child("Users").orderByKey();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        for (DataSnapshot childsnapShot : snapshot.child("viralmarketing").getChildren()) {
                            if (user_email.equals(childsnapShot.getValue().toString())) {
                                childsnapShot.getRef().setValue("done");
                                int beforeurbees = Integer.parseInt(snapshot.child("urbees").getValue().toString());
                                int urbeesnew = beforeurbees + 100;
                                snapshot.child("urbees").getRef().setValue(urbeesnew);
                            }
                        }

                    }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });



    }
    public void Okdialog(String title, String information){
        welcomingDialog1= new Dialog(activity1);
        welcomingDialog1.setContentView(R.layout.popup_notification);

        dialoginformation = welcomingDialog1.findViewById(R.id.text_description);
        dialogtitle = welcomingDialog1.findViewById(R.id.information_text);
        dialog_button= welcomingDialog1.findViewById(R.id.button_ok);

        dialogtitle.setText(title);
        dialoginformation.setText(information);

        dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                welcomingDialog1.dismiss();
            }
        });
        welcomingDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        welcomingDialog1.show();
    }
    public void click_claimurbees_function(){
        if (urbees_total != 0) {
            claim_Urbees.setEnabled(false);
            claim_Urbees_taptext.setEnabled(false);
            reference = FirebaseDatabase.getInstance().getReference().child("Users").child(user.getUid());

            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    int OldUrbees = Integer.parseInt(dataSnapshot.child("urbees").getValue().toString());
                    int OldMobilityTimes = Integer.parseInt(dataSnapshot.child("mobilitytimes").getValue().toString());
                    int newMobilityTimes = OldMobilityTimes + 1;
                    int newUrbees = OldUrbees + urbees_total;
                    reference.child("urbees").setValue(newUrbees);
                    user_urbees_dopaminepage.setText(""+newUrbees);
                    reference.child("mobilitytimes").setValue(newMobilityTimes);

                    Okdialog(getString(R.string.congratulations_urbees_erned_dopamine_title1) + " " + urbees_total + " " + getString(R.string.congratulations_urbees_erned_dopamine_title2) + " " + newUrbees + "!",getString(R.string.congratulations_urbees_erned_dopamine_text));


                    SharedPreferences sharedPreferences = activity1.getSharedPreferences("Pointshandling", activity1.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("urbees_bike", 0);
                    editor.putInt("urbees_foot", 0);
                    editor.apply();
                    urbees_bike = sharedPreferences.getInt("urbees_bike", 0);
                    urbees_foot = sharedPreferences.getInt("urbees_foot", 0);
                    urbees_total = urbees_bike + urbees_foot;
                    total_text.setText(""+urbees_total);
                    claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage0);
                    claim_Urbees.setEnabled(true);
                    claim_Urbees_taptext.setEnabled(true);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(activity1, databaseError.getMessage(), Toast.LENGTH_LONG).show();
                    claim_Urbees.setEnabled(true);
                    claim_Urbees_taptext.setEnabled(true);
                }
            });
        }//end of if total urbees
        else {
            claim_Urbees.setImageResource(R.drawable.dopaminbeehivestage0);
            Okdialog(getString(R.string.dopamine_no_urbees_title),getString(R.string.dopamine_no_urbees_text));
        }


    }
}
