package com.urbanbees.nextorbis.urbanbeesmobility.profile.viralmarketing;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.urbanbees.nextorbis.urbanbeesmobility.R;

public class ViralActivity extends AppCompatActivity {
    FirebaseAuth auth;
    FirebaseUser user;
    DatabaseReference reference, reference2;
    TextView txtName_profile_viral, txtinfo_viral,btviral_text ;
    ImageButton invite_viral;
    EditText email_viral;
    String email_viral_text,user_id;
    AlertDialog.Builder invitation_dialog;
    boolean viralexist= false, viralinvitedexist=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viral);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        user_id=user.getUid();

        invite_viral=  findViewById(R.id.btviral);
        email_viral= findViewById(R.id.etEmail_viral);
        txtName_profile_viral= findViewById(R.id.txtName_profile_viral);
        txtinfo_viral= findViewById(R.id.txtinfo_viral);
        btviral_text=findViewById(R.id.btviral_text);
        //TextViewCompat.setAutoSizeTextTypeWithDefaults(txtName_profile_viral,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        //TextViewCompat.setAutoSizeTextTypeWithDefaults(txtinfo_viral,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(txtName_profile_viral,70,100,1, TypedValue.COMPLEX_UNIT_PX);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(txtinfo_viral,50,80,1, TypedValue.COMPLEX_UNIT_PX);

        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(btviral_text,35,70,1, TypedValue.COMPLEX_UNIT_PX);

        //set text initialize
        txtName_profile_viral.setText(R.string.viral_text_title);


        //Alert dialog for when the invite is done
        invitation_dialog = new android.support.v7.app.AlertDialog.Builder(ViralActivity.this,R.style.AlertDialogTheme);
        invitation_dialog.setMessage(getString(R.string.inivite_viral_text)).setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        invite_viral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email_viral_text= email_viral.getText().toString().toLowerCase();
                if(email_viral_text.equals("")){
                    Toast.makeText(ViralActivity.this, "Blank not allowed", Toast.LENGTH_SHORT).show();
                }else{

                    sendEmailViral();
                }
            }
        });

    }

    public void sendEmailViral () {
        //check if the user already is in the app
      Query query = FirebaseDatabase.getInstance().getReference().child("Users").orderByChild("useremail").equalTo(email_viral_text);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getChildrenCount() > 0) {
                    viralexist=true;
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(ViralActivity.this, databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        //check if the user already is invited
        Query query2 = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("viralmarketing").orderByValue().equalTo(email_viral_text);
        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getChildrenCount() > 0) {
                    viralinvitedexist=true;
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(ViralActivity.this, databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


        // Add the viral marketing email
        reference2 = FirebaseDatabase.getInstance().getReference();
        reference2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                if(viralinvitedexist){
                    Toast.makeText(ViralActivity.this, "You already invited this Bee", Toast.LENGTH_SHORT).show();
                    viralinvitedexist=false;
                }else if(viralexist){
                    Toast.makeText(ViralActivity.this, "This Bee is already part of the hive", Toast.LENGTH_SHORT).show();
                    viralexist=false;
                }else{
                   reference2.child("Users").child(user_id).child("viralmarketing").push().setValue(email_viral_text);
                    email_viral.setText("");
                    String subject= user.getDisplayName()+" invited you to download UrbanBees!";
                    String message= "<p>Hey there,</p> <p>Your friend "+user.getDisplayName()+" wants you to be part of the UrbanBees Community!</p>" +System.getProperty("line.separator")+
                            "<p>Go to the link below to download the app for free!</p>" +System.getProperty("line.separator")+
                            "https://play.google.com/store/apps/details?id=com.urbanbees.nextorbis.urbanbeesmobility" +System.getProperty("line.separator")+
                            "" +System.getProperty("line.separator")+
                            "<p>Oh, you don't know what UrbanBees is? Well, in a few words UrbanBees is the app that rewards you for saving the planet. If you want to see how it works you can watch this short video!</p> https://nextorbis.com/urbanbees/";
                    sendEmailtoPotencialNewBee sm= new sendEmailtoPotencialNewBee(getApplicationContext(),email_viral_text, subject, message);
                    sm.execute();

                    AlertDialog alert = invitation_dialog.create();
                    alert.setTitle(getString(R.string.viral_text_title_invite));
                    alert.show();

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ViralActivity.this, databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }


}
